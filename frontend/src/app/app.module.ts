import { APP_INITIALIZER, ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxsModule } from '@ngxs/store';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { HttpClientModule } from '@angular/common/http';
import { AdminState } from './pages/admin-page/_store/admin.state';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { MailinglistState } from './pages/mailinglist-page/_store/mailinglists.state';
import { WorkgroupsState } from './pages/wg-member-page/_store/workgroups.state';
import { ConfirmDialogState } from './components/confirm-dialog/_state/confirm-dialog.state';
import { NgxsSelectSnapshotModule } from '@ngxs-labs/select-snapshot';
import { UiState } from './_store/ui.state';
import { CustomErrorHandler } from './utils/CustomErrorHandler';
import {
  NgxsStoragePlugin,
  NgxsStoragePluginModule,
} from '@ngxs/storage-plugin';
import { AuthState } from './_store/auth.state';

function initializeKeycloak(keycloak: KeycloakService) {
  return () =>
    keycloak.init({
      config: '/assets/keycloak.json',
      initOptions: {
        onLoad: 'login-required',
        //        silentCheckSsoRedirectUri:
        //        window.location.origin + '/assets/silent-check-sso.html',
      },
    });
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgxsModule.forRoot([
      AdminState,
      MailinglistState,
      WorkgroupsState,
      ConfirmDialogState,
      UiState,
      AuthState,
    ]),
    NgxsRouterPluginModule.forRoot(),
    NgxsStoragePluginModule.forRoot({ key: 'ui.darkMode' }),
    NgxsFormPluginModule.forRoot(),
    NgxsReduxDevtoolsPluginModule.forRoot({
      stateSanitizer: state => {
        return {
          ...state,
          admin: {
            ...state.admin,
            dialog: undefined,
          },
          workgroups: {
            ...state.dialog,
            dialog: undefined,
          },
          confirmDialog: {
            ...state.dialog,
            dialog: undefined,
          },
        };
      },
    }),
    NgxsLoggerPluginModule.forRoot(),
    NgxsSelectSnapshotModule.forRoot(),
    KeycloakAngularModule,
    HttpClientModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    },
    { provide: ErrorHandler, useClass: CustomErrorHandler },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
