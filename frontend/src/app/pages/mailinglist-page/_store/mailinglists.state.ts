import { Injectable } from '@angular/core';
import { Action, State, StateContext, StateToken } from '@ngxs/store';
import { UserMailinglistUserDto } from 'src/app/api/models';
import { ApiService } from 'src/app/api/services';
import { MailinglistActions } from './mailinglists.actions';

export interface UserStateModel {
  mailinglists: UserMailinglistUserDto[];
}

export const USER_STATE_TOKEN = new StateToken<UserStateModel>('user');

@State({
  name: USER_STATE_TOKEN,
  defaults: {
    mailinglists: [],
  },
})
@Injectable()
export class MailinglistState {
  constructor(private apiService: ApiService) {}

  @Action(MailinglistActions.GetUserMailinglists)
  public async getUserMailinglists({
    patchState,
  }: StateContext<UserStateModel>) {
    const response = await this.apiService
      .getApiV1UserMailinglistsResponse()
      .toPromise();
    if (response.status === 200) {
      patchState({
        mailinglists: response.body,
      });
    }
  }

  @Action(MailinglistActions.JoinMailinglist)
  public async joinMailinglist(
    { dispatch }: StateContext<UserStateModel>,
    action: MailinglistActions.JoinMailinglist,
  ) {
    const response = await this.apiService
      .postApiV1UserMailinglistUuidJoinResponse(action.mailinglist.uuid!)
      .toPromise();
    dispatch(MailinglistActions.GetUserMailinglists);
  }

  @Action(MailinglistActions.LeaveMailinglist)
  public async leaveMailinglist(
    { dispatch }: StateContext<UserStateModel>,
    action: MailinglistActions.LeaveMailinglist,
  ) {
    const response = await this.apiService
      .postApiV1UserMailinglistUuidLeaveResponse(action.mailinglist.uuid!)
      .toPromise();
    dispatch(MailinglistActions.GetUserMailinglists);
  }
}
