import { UserMailinglistUserDto } from 'src/app/api/models';

export namespace MailinglistActions {
  export class GetUserMailinglists {
    static readonly type = '[User] GetUserMailinglists';
  }

  export class JoinMailinglist {
    static readonly type = '[User] JoinMailinglist';
    constructor(public mailinglist: UserMailinglistUserDto) {}
  }

  export class LeaveMailinglist {
    static readonly type = '[User] LeaveMailinglist';
    constructor(public mailinglist: UserMailinglistUserDto) {}
  }
}
