import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MailinglistPageComponent } from './mailinglist-page.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { CommonImportsModule } from 'src/app/common-imports/common-imports.module';
@NgModule({
  declarations: [MailinglistPageComponent],
  imports: [CommonModule, ComponentsModule, CommonImportsModule],
})
export class MailinglistPageModule {}
