import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserMailinglistUserDto } from 'src/app/api/models';
import { MailinglistActions } from './_store/mailinglists.actions';
import { UserStateModel } from './_store/mailinglists.state';

@Component({
  selector: 'app-mailinglist-page',
  templateUrl: './mailinglist-page.component.html',
  styleUrls: ['./mailinglist-page.component.scss'],
})
export class MailinglistPageComponent implements OnInit {
  @Select((state: { user: UserStateModel }) => state.user.mailinglists)
  public mailinglists$!: Observable<UserMailinglistUserDto[]>;

  public validMailinglists$?: Observable<UserMailinglistUserDto[]>;

  constructor(private store: Store) {}

  public ngOnInit() {
    this.validMailinglists$ = this.mailinglists$.pipe(
      map(mailinglists => mailinglists.filter(ml => ml.valid)),
    );
    this.store.dispatch(new MailinglistActions.GetUserMailinglists());
  }
}
