import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MailPickerComponent } from './mail-picker.component';

describe('MailPickerComponent', () => {
  let component: MailPickerComponent;
  let fixture: ComponentFixture<MailPickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MailPickerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MailPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
