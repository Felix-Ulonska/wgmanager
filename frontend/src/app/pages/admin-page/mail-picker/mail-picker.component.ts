import { Component, Input, OnInit } from '@angular/core';
import { ControlContainer, FormGroupDirective } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AdminAvailableMaillistsDto } from 'src/app/api/models';
import { AdminActions } from '../_store/admin.actions';
import { AdminStateModel } from '../_store/admin.state';

@Component({
  selector: 'app-mail-picker',
  templateUrl: './mail-picker.component.html',
  styleUrls: ['./mail-picker.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class MailPickerComponent implements OnInit {
  @Input()
  public _formControlName = '';

  @Input()
  public allowEmpty = false;

  @Select((state: { admin: AdminStateModel }) => state.admin.availableMails)
  public availableMails$!: Observable<AdminAvailableMaillistsDto[]>;

  @Input()
  public currentMail: string | undefined = '';

  constructor(private store: Store, public parent: FormGroupDirective) {}

  ngOnInit(): void {
    this.store.dispatch(new AdminActions.GetAvailableMails());
  }
}
