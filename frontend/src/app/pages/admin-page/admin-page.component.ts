import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import {
  UserMailinglistUserDto,
  UserWorkgroupUserDto,
} from 'src/app/api/models';
import { MailinglistActions } from '../mailinglist-page/_store/mailinglists.actions';
import { UserStateModel } from '../mailinglist-page/_store/mailinglists.state';
import { WorkgroupActions } from '../wg-member-page/_store/workgroups.actions';
import { WorkgroupsStateModel } from '../wg-member-page/_store/workgroups.state';
import { AdminActions } from './_store/admin.actions';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.scss'],
})
export class AdminPageComponent implements OnInit {
  @Select((state: { user: UserStateModel }) => state.user.mailinglists)
  public mailinglists$!: Observable<UserMailinglistUserDto[]>;

  @Select(
    (state: { workgroups: WorkgroupsStateModel }) =>
      state.workgroups.workgroups,
  )
  public workgroups$!: Observable<UserWorkgroupUserDto[]>;

  constructor(private readonly store: Store) {}

  public ngOnInit() {
    this.store.dispatch(new AdminActions.GetAdminMailinglists());
    this.store.dispatch(new MailinglistActions.GetUserMailinglists());
    this.store.dispatch(new WorkgroupActions.GetWorkgroups());
    this.store.dispatch(new AdminActions.GetPossibleGroups());
    this.store.dispatch(new AdminActions.GetAllWorkgroups());
  }

  public addMailinglist() {
    this.store.dispatch(new AdminActions.OpenAddMailinglistModal());
  }

  public addWorkgroup() {
    this.store.dispatch(new AdminActions.OpenAddWorkgroupDialog());
  }
}
