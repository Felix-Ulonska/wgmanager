import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ModelsWorkgroup } from 'src/app/api/models';
import { BaseComponent } from 'src/app/components/base/base.component';
import { WorkgroupActions } from '../../wg-member-page/_store/workgroups.actions';
import { AdminActions } from '../_store/admin.actions';
import { AdminStateModel } from '../_store/admin.state';

@Component({
  selector: 'app-wg-list',
  templateUrl: './wg-list.component.html',
  styleUrls: ['./wg-list.component.scss'],
})
export class WgListComponent extends BaseComponent implements OnInit {
  public displayedColumns = ['name', 'email', 'editButton', 'editMembers'];

  @Select((state: { admin: AdminStateModel }) => state.admin.workgroups)
  public possibleGroups$!: Observable<ModelsWorkgroup[]>;

  public dataSource = new MatTableDataSource<ModelsWorkgroup>();

  constructor(private store: Store) {
    super();
  }

  public ngOnInit() {
    this.store.dispatch(new AdminActions.GetAllWorkgroups());
    this.possibleGroups$
      .pipe(takeUntil(this.onDestory$))
      .subscribe(mailinglists => (this.dataSource.data = mailinglists));
  }

  public edit(workgroup: ModelsWorkgroup) {
    this.store.dispatch(new AdminActions.OpenEditWorkgroupDialog(workgroup));
  }

  public openMembers(wg: ModelsWorkgroup) {
    this.store.dispatch(new WorkgroupActions.OpenMembersDialog(wg.uuid!));
  }
}
