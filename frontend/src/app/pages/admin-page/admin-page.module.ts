import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminPageComponent } from './admin-page.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { CommonImportsModule } from 'src/app/common-imports/common-imports.module';
import { MailinglistConfigDialogComponent } from './dialogs/mailinglist-config-dialog/mailinglist-config-dialog.component';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';
import { MlListComponent } from './ml-list/ml-list.component';
import { WorkgroupConfigDialogComponent } from './dialogs/workgroup-config-dialog/workgroup-config-dialog.component';
import { WgListComponent } from './wg-list/wg-list.component';
import { MailPickerComponent } from './mail-picker/mail-picker.component';

@NgModule({
  declarations: [
    AdminPageComponent,
    MailinglistConfigDialogComponent,
    MlListComponent,
    WorkgroupConfigDialogComponent,
    WgListComponent,
    MailPickerComponent,
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    CommonImportsModule,
    NgxsFormPluginModule,
  ],
})
export class AdminPageModule {}
