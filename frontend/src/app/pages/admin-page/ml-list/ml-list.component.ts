import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ModelsMailinglist } from 'src/app/api/models';
import { BaseComponent } from 'src/app/components/base/base.component';
import { AdminActions } from '../_store/admin.actions';
import { AdminStateModel } from '../_store/admin.state';

@Component({
  selector: 'app-ml-list',
  templateUrl: './ml-list.component.html',
  styleUrls: ['./ml-list.component.scss'],
})
export class MlListComponent extends BaseComponent implements OnInit {
  public displayedColumns = ['name', 'email', 'editButton'];

  @Select((state: { admin: AdminStateModel }) => state.admin.mailinglists)
  public possibleGroups$!: Observable<ModelsMailinglist[]>;

  public dataSource = new MatTableDataSource<ModelsMailinglist>();

  constructor(private store: Store) {
    super();
  }

  public ngOnInit() {
    this.possibleGroups$
      .pipe(takeUntil(this.onDestory$))
      .subscribe(mailinglists => (this.dataSource.data = mailinglists));
  }

  public editMailinglist(mailinglist: ModelsMailinglist) {
    this.store.dispatch(new AdminActions.OpenEditMailinglistModal(mailinglist));
  }
}
