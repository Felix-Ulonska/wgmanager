import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MlListComponent } from './ml-list.component';

describe('MlListComponent', () => {
  let component: MlListComponent;
  let fixture: ComponentFixture<MlListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MlListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MlListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
