import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { ModelsLdapGroup, ModelsWorkgroup } from 'src/app/api/models';
import { ConfirmDialogActions } from 'src/app/components/confirm-dialog/_state/confirm-dialog.action';
import { WorkgroupActions } from 'src/app/pages/wg-member-page/_store/workgroups.actions';
import { WorkgroupsState } from 'src/app/pages/wg-member-page/_store/workgroups.state';
import { AdminActions } from '../../_store/admin.actions';
import { AdminStateModel, ModalState } from '../../_store/admin.state';

@Component({
  selector: 'app-workgroup-config-dialog',
  templateUrl: './workgroup-config-dialog.component.html',
  styleUrls: ['./workgroup-config-dialog.component.scss'],
})
export class WorkgroupConfigDialogComponent implements OnInit {
  ModalState = ModalState;

  @Select((state: { admin: AdminStateModel }) => state.admin.possibleGroups)
  possibleGroups!: Observable<ModelsLdapGroup[]>;

  @Select(
    (state: { admin: AdminStateModel }) => state.admin.workgroupConfigForm,
  )
  config!: Observable<AdminStateModel>;

  @Select(
    (state: { admin: AdminStateModel }) =>
      state.admin.workgroupConfigForm.status,
  )
  status!: Observable<string>;

  @Select(
    (state: { admin: AdminStateModel }) => state.admin.workgroupModalState,
  )
  modalState!: Observable<ModalState>;

  @Select(
    (state: { admin: AdminStateModel }) => state.admin.workgroupModalEditing,
  )
  oldWorkgroup!: Observable<ModelsWorkgroup>;

  workgroupConfigForm = new FormGroup({
    allowJoinLdapGroup: new FormControl(null, [Validators.required]),
    excludeMailinglistLdapGroup: new FormControl(null, [Validators.required]),
    mail: new FormControl(null, []),
    matterMostLink: new FormControl(null, []),
    memberLdapGroup: new FormControl(null, [Validators.required]),
    speakerLdapGroup: new FormControl(null, [Validators.required]),
    name: new FormControl(null, [Validators.required]),
  });

  constructor(private readonly store: Store) {}

  ngOnInit(): void {
    this.store.dispatch(new AdminActions.GetPossibleGroups());
  }

  public async submit() {
    const workgroup = this.workgroupConfigForm.value;
    if (this.workgroupConfigForm.valid) {
      if (
        (await this.modalState.pipe(first()).toPromise()) === ModalState.Add
      ) {
        this.store.dispatch(
          new ConfirmDialogActions.Open({
            title: `${this.workgroupConfigForm.value.name} hinzufügen?`,
            body: `Soll die Arbeitgruppe ${this.workgroupConfigForm.value.name} hinzugefügt. Wenn die Arbeitgruppe hinzugefügt wird, werde alle bestehenden Mitglieder aus der Mailingliste ${this.workgroupConfigForm.value.mail} überschrieben. Dies ist nicht umkehrbar!`,
            actionOnConfirm: new AdminActions.AddWorkgroup(workgroup),
          }),
        );
      } else {
        let body = `Soll die Arbeitgruppe ${this.workgroupConfigForm.value.name} bearbeitet werden?`;
        if (
          (await this.oldWorkgroup.pipe(first()).toPromise()).mail !==
          this.workgroupConfigForm.value.mail
        ) {
          body += `Es wurde die Mailadresse geändert. Bestehende Mitglieder werden nicht aus der alten E-Mail Liste entfernt. Alle Mitglieder aus der neuen Liste ${this.workgroupConfigForm.value.mail} werden überschrieben. Dies ist nicht umkehrbar!`;
        }
        this.store.dispatch(
          new ConfirmDialogActions.Open({
            title: `Mailingliste ${this.workgroupConfigForm.value.name} bearbeiten`,
            body: body,
            actionOnConfirm: new AdminActions.EditWorkgroup({
              ...workgroup,
              uuid: (this.store.snapshot().admin as AdminStateModel)
                .workgroupModalEditing!.uuid,
            }),
          }),
        );
      }
    }
  }

  public delete() {
    this.store.dispatch(
      new ConfirmDialogActions.Open({
        title: `${this.workgroupConfigForm.value.name} entfernen?`,
        body: `Soll die ${this.workgroupConfigForm.value.name} entfernt werden? Dies wird die Liste nicht in mailman löschen. Bestehende Mitglieder werden nicht entfernt!`,
        actionOnConfirm: new AdminActions.DeleteWorkgroup(
          this.store.snapshot().admin.workgroupModalEditing!,
        ),
      }),
    );
  }

  public close() {
    this.store.dispatch(new AdminActions.CloseModal());
  }
}
