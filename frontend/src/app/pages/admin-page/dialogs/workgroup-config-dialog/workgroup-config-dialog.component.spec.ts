import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkgroupConfigDialogComponent } from './workgroup-config-dialog.component';

describe('WorkgroupConfigDialogComponent', () => {
  let component: WorkgroupConfigDialogComponent;
  let fixture: ComponentFixture<WorkgroupConfigDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkgroupConfigDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkgroupConfigDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
