import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { ModelsLdapGroup, ModelsMailinglist } from 'src/app/api/models';
import { ConfirmDialogActions } from 'src/app/components/confirm-dialog/_state/confirm-dialog.action';
import {
  AdminStateModel,
  ModalState,
} from 'src/app/pages/admin-page/_store/admin.state';
import { MailinglistActions } from 'src/app/pages/mailinglist-page/_store/mailinglists.actions';
import { AdminActions } from '../../_store/admin.actions';

@Component({
  selector: 'app-mailinglist-config-dialog',
  templateUrl: './mailinglist-config-dialog.component.html',
  styleUrls: ['./mailinglist-config-dialog.component.scss'],
})
export class MailinglistConfigDialogComponent implements OnInit {
  public MailinglistModalState = ModalState;

  @Select((state: { admin: AdminStateModel }) => state.admin.possibleGroups)
  possibleGroups!: Observable<ModelsLdapGroup[]>;

  @Select((state: { admin: AdminStateModel }) => state.admin.mailConfigForm)
  config!: Observable<AdminStateModel>;

  @Select(
    (state: { admin: AdminStateModel }) => state.admin.mailConfigForm.status,
  )
  status!: Observable<string>;

  @Select(
    (state: { admin: AdminStateModel }) => state.admin.mailinglistModalState,
  )
  modalState!: Observable<ModalState>;

  @Select(
    (state: { admin: AdminStateModel }) => state.admin.mailnglistModalEditing,
  )
  oldMailinglist!: Observable<ModelsMailinglist>;

  mailConfigForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    baseGroup: new FormControl(null, [Validators.required]),
    modifierGroup: new FormControl(null, [Validators.required]),
    modifierGroupIsInclude: new FormControl(false, [Validators.required]),
  });

  get modifierGroupIsInclude(): boolean {
    return this.mailConfigForm?.value?.modifierGroupIsInclude ?? false;
  }

  constructor(private readonly store: Store) {}

  ngOnInit() {
    this.store.dispatch([
      new AdminActions.GetPossibleGroups(),
      new MailinglistActions.GetUserMailinglists(),
    ]);
  }

  async submit() {
    this.mailConfigForm.markAllAsTouched();
    if (
      (this.store.snapshot().admin as AdminStateModel).mailinglistModalState ===
      ModalState.Edit
    ) {
      console.log('Sending Edit');
      let body = `Soll die Mailingliste ${this.mailConfigForm.value.name} bearbeitet werden.`;
      if (
        (await this.oldMailinglist.pipe(first()).toPromise()).email !==
        this.mailConfigForm.value.email
      ) {
        body += `Es wurde die Mailadresse geändert. Bestehende Mitglieder werden nicht aus der alten E-Mail Liste entfernt. Alle Mitglieder aus der neuen Liste ${this.mailConfigForm.value.email} werden überschrieben. Dies ist nicht umkehrbar!`;
      }
      this.store.dispatch(
        new ConfirmDialogActions.Open({
          title: `Mailingliste ${this.mailConfigForm.value.name} bearbeiten`,
          body: body,
          actionOnConfirm: new AdminActions.EditMailinglist({
            uuid: (this.store.snapshot().admin as AdminStateModel)
              .mailnglistModalEditing!.uuid,
            name: this.mailConfigForm.value.name,
            baseGroup: this.mailConfigForm.value.baseGroup.id,
            modifierGroup: this.mailConfigForm.value.modifierGroup.id,
            email: this.mailConfigForm.value.email,
            modifierGroupIsInclude:
              this.mailConfigForm.value.modifierGroupIsInclude,
          }),
        }),
      );
    } else {
      console.log('Sending Add');
      this.store.dispatch(
        new ConfirmDialogActions.Open({
          title: `Mailingliste ${this.mailConfigForm.value.name} hinzufügen`,
          body: `Soll die Mailingliste ${this.mailConfigForm.value.name} hinzugefügt. Wenn die Mailingliste hinzugefügt wird, werde alle bestehenden Mitglieder aus der Mailingliste ${this.mailConfigForm.value.email} überschrieben. Dies ist nicht umkehrbar!`,
          actionOnConfirm: new AdminActions.AddMailinglist({
            name: this.mailConfigForm.value.name,
            baseGroup: this.mailConfigForm.value.baseGroup.id,
            modifierGroup: this.mailConfigForm.value.modifierGroup.id,
            email: this.mailConfigForm.value.email,
            modifierGroupIsInclude:
              this.mailConfigForm.value.modifierGroupIsInclude,
          }),
        }),
      );
    }
  }

  public closeModal() {
    this.store.dispatch(new AdminActions.CloseModal());
  }

  public delete() {
    this.store.dispatch(
      new ConfirmDialogActions.Open({
        title: `${this.mailConfigForm.value.name} entfernen?`,
        body: `Soll die ${this.mailConfigForm.value.name} entfernt werden? Dies wird die Liste nicht in mailman löschen. Bestehende Mitglieder werden nicht entfernt!`,
        actionOnConfirm: new AdminActions.DeleteMailinglist(
          (
            this.store.snapshot().admin as AdminStateModel
          ).mailnglistModalEditing!.uuid!,
        ),
      }),
    );
  }

  public compareLdapGroups(g1: ModelsLdapGroup, g2: ModelsLdapGroup) {
    return g1.id === g2.id;
  }
}
