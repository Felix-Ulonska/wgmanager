import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MailinglistConfigDialogComponent } from './mailinglist-config-dialog.component';

describe('MailinglistConfigDialogComponent', () => {
  let component: MailinglistConfigDialogComponent;
  let fixture: ComponentFixture<MailinglistConfigDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MailinglistConfigDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MailinglistConfigDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
