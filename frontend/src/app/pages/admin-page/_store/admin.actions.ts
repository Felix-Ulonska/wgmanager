import {
  AdminWorkgroupAddDto,
  ModelsLdapGroup,
  ModelsMailinglist,
  ModelsWorkgroup,
} from 'src/app/api/models';

export namespace AdminActions {
  export class OpenAddMailinglistModal {
    static readonly type = '[Admin] Add Mailinglist';
  }

  export class OpenEditMailinglistModal {
    static readonly type = '[Admin] Edit Mailinglist';
    constructor(public mailinglist: ModelsMailinglist) {}
  }

  export class GetPossibleGroups {
    static readonly type = '[Admin] GetPossibleGroups';
  }

  export class GetAvailableMails {
    static readonly type = '[Admin] GetAvailableMails';
  }

  export class GetAdminMailinglists {
    static readonly type = '[Admin] GetAdminMailinglists';
  }

  export class AddMailinglist {
    static readonly type = '[Admin] AddMailinglist';

    constructor(
      public mailinglist: {
        name: string;
        baseGroup: ModelsLdapGroup;
        modifierGroup: ModelsLdapGroup;
        modifierGroupIsInclude: boolean;
        email: string;
      },
    ) {}
  }

  export class DeleteMailinglist {
    static readonly type = '[Admin] DeleteMailinglist';
    constructor(public uuid: string) {}
  }

  export class EditMailinglist {
    static readonly type = '[Admin] EditMailinglist';

    constructor(public mailinglist: ModelsMailinglist) {}
  }

  export class CloseModal {
    static readonly type = '[Admin] CloseModal';
  }

  export class AddWorkgroup {
    static readonly type = '[Admin] AddWorkgroup';
    constructor(public workgroup: AdminWorkgroupAddDto) {}
  }

  export class AddSpeakerToWorkgroup {
    static readonly type = '[Admin] AddSpeakerToWorkgroup';
    constructor(public workgroupId: string, public userId: string) {}
  }

  export class RemoveSpeakerFromWorkgroup {
    static readonly type = '[Admin] RemoveSpeakerFromWorkgroup';
    constructor(public workgroupId: string, public userId: string) {}
  }

  export class OpenAddWorkgroupDialog {
    static readonly type = '[Admin] OpenAddWorkgroupDialog';
  }

  export class OpenEditWorkgroupDialog {
    static readonly type = '[Admin] OpenEditWorkgroupDialog';
    constructor(public workgroup: ModelsWorkgroup) {}
  }

  export class EditWorkgroup {
    static readonly type = '[Admin] EditWorkgroup';

    constructor(public workgroup: ModelsWorkgroup) {}
  }

  export class GetAllWorkgroups {
    static readonly type = '[Admin] GetAllWorkgroups';
  }

  export class DeleteWorkgroup {
    static readonly type = '[Admin] DeleteWorkgroup';

    constructor(public workgroup: ModelsWorkgroup) {}
  }
}
