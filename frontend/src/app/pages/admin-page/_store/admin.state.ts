import { HttpResponse } from '@angular/common/http';
import { Injectable, NgZone } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ResetForm } from '@ngxs/form-plugin';
import { Action, State, StateContext, StateToken } from '@ngxs/store';
import { first } from 'rxjs/operators';
import {
  AdminAvailableMaillistsDto,
  ModelsLdapGroup,
  ModelsMailinglist,
  ModelsWorkgroup,
} from 'src/app/api/models';
import { ApiService } from 'src/app/api/services';
import { MailinglistActions } from '../../mailinglist-page/_store/mailinglists.actions';
import { WorkgroupActions } from '../../wg-member-page/_store/workgroups.actions';
import { MailinglistConfigDialogComponent } from '../dialogs/mailinglist-config-dialog/mailinglist-config-dialog.component';
import { WorkgroupConfigDialogComponent } from '../dialogs/workgroup-config-dialog/workgroup-config-dialog.component';
import { AdminActions } from './admin.actions';

export enum ModalState {
  Add,
  Edit,
}

export interface AdminStateModel {
  possibleGroups: ModelsLdapGroup[];
  availableMails: AdminAvailableMaillistsDto[];
  dialog?: MatDialogRef<any>;
  mailinglists?: ModelsMailinglist[];
  workgroups?: ModelsWorkgroup[];
  mailinglistModalState?: ModalState;
  mailnglistModalEditing?: ModelsMailinglist;
  mailConfigForm: {
    model?: {
      name: string;
      baseGroup: ModelsLdapGroup;
      modifierGroup: ModelsLdapGroup;
      modifierGroupIsInclude: boolean;
      email: string;
    };
    dirty: false;
    status: '';
    errors: {};
  };
  workgroupConfigForm: {
    model?: {
      allowJoinLdapGroup: string;
      excludeMailinglistLdapGroup: string;
      mail?: string;
      matterMostLink?: string;
      memberLdapGroup: string;
      speakerLdapGroup: string;
      name: string;
    };
    dirty: false;
    status: '';
    errors: {};
  };
  workgroupModalState?: ModalState;
  workgroupModalEditing?: ModelsWorkgroup;
}

export const ADMIN_STATE_TOKEN = new StateToken<AdminStateModel>('admin');

@State({
  name: ADMIN_STATE_TOKEN,
  defaults: {
    possibleGroups: [],
    availableMails: [],
    mailConfigForm: {
      model: undefined,
      dirty: false,
      status: '',
      errors: {},
    },
    dialog: undefined,
    workgroupConfigForm: {
      model: undefined,
      dirty: false,
      status: '',
      errors: {},
    },
  },
})
@Injectable()
export class AdminState {
  constructor(
    private readonly ngZone: NgZone,
    private readonly dialog: MatDialog,
    private readonly apiService: ApiService,
  ) {}

  @Action(AdminActions.OpenAddMailinglistModal)
  openAddMaillinglustModal({ patchState }: StateContext<AdminStateModel>) {
    this.ngZone.run(() => {
      patchState({
        mailinglistModalState: ModalState.Add,
        dialog: this.dialog.open(MailinglistConfigDialogComponent, {
          width: '40em',
          maxWidth: '100%',
          maxHeight: '100vh',
          disableClose: true,
        }),
      });
    });
  }

  @Action(AdminActions.OpenAddWorkgroupDialog)
  openWorkgroupDialog({ patchState }: StateContext<AdminStateModel>) {
    this.ngZone.run(() => {
      patchState({
        workgroupModalState: ModalState.Add,
        workgroupModalEditing: undefined,
        dialog: this.dialog.open(WorkgroupConfigDialogComponent, {
          width: '40em',
          maxWidth: '100%',
          maxHeight: '100vh',
          disableClose: true,
        }),
      });
    });
  }

  @Action(AdminActions.OpenEditMailinglistModal)
  async openEditMaillinglistModal(
    { patchState, getState, dispatch }: StateContext<AdminStateModel>,
    action: AdminActions.OpenEditMailinglistModal,
  ) {
    dispatch(new AdminActions.GetPossibleGroups())
      .pipe(first())
      .subscribe(() => {
        this.ngZone.run(() => {
          patchState({
            mailnglistModalEditing: action.mailinglist,
            mailinglistModalState: ModalState.Edit,
            dialog: this.dialog.open(MailinglistConfigDialogComponent, {
              width: '40em',
              maxWidth: '100%',
              maxHeight: '100%',
              disableClose: true,
            }),
            mailConfigForm: {
              ...getState().mailConfigForm,
              model: {
                name: action.mailinglist.name,
                email: action.mailinglist.email,
                baseGroup: getState().possibleGroups.find(
                  group => group.id === action.mailinglist.baseGroup,
                )!,
                modifierGroup: getState().possibleGroups.find(
                  group => group.id === action.mailinglist.modifierGroup,
                )!,
                modifierGroupIsInclude:
                  action.mailinglist.modifierGroupIsInclude ?? false,
              },
            },
          });
        });
      });
  }

  @Action(AdminActions.GetPossibleGroups)
  public async getPossibleGroups({
    patchState,
  }: StateContext<AdminStateModel>) {
    patchState({
      possibleGroups: await this.apiService
        .getApiV1AdminAllAvailableGroups()
        .toPromise(),
    });
  }

  @Action(AdminActions.GetAvailableMails)
  public async getAvaibleMails({ patchState }: StateContext<AdminStateModel>) {
    patchState({
      availableMails: await this.apiService
        .getApiV1AdminAvailableMaillistFromMailman()
        .toPromise(),
    });
  }

  @Action(AdminActions.GetAdminMailinglists)
  public async getMailinglists({
    patchState,
    dispatch,
  }: StateContext<AdminStateModel>) {
    const response = await this.apiService
      .getApiV1AdminMailinglistsResponse()
      .toPromise();
    if (response.status === 200) {
      patchState({ mailinglists: response.body });
    }
    dispatch(new MailinglistActions.GetUserMailinglists());
  }

  @Action(AdminActions.AddMailinglist)
  public async addMailinglist(
    { dispatch }: StateContext<AdminStateModel>,
    action: AdminActions.AddMailinglist,
  ) {
    const response = await this.apiService
      .postApiV1AdminMailinglistResponse({
        email: action.mailinglist.email,
        name: action.mailinglist.name,
        baseGroup: action.mailinglist.baseGroup!.id!,
        modifierGroup: action.mailinglist.modifierGroup!.id!,
        modifierGroupIsInclude: action.mailinglist.modifierGroupIsInclude,
      })
      .toPromise();
    if (response.status === 201) {
      dispatch([
        new AdminActions.CloseModal(),
        new AdminActions.GetAdminMailinglists(),
      ]);
    }
  }

  @Action(AdminActions.DeleteMailinglist)
  public async deleteMailinglist(
    { dispatch }: StateContext<AdminStateModel>,
    action: AdminActions.DeleteMailinglist,
  ) {
    const response = await this.apiService
      .deleteApiV1AdminMailinglistUuidResponse(action.uuid)
      .toPromise();
    if (response.status === 200) {
      dispatch(new AdminActions.CloseModal());
    }
    dispatch(new AdminActions.GetAdminMailinglists());
  }

  @Action(AdminActions.EditMailinglist)
  public async editMailinglist(
    { dispatch }: StateContext<AdminStateModel>,
    action: AdminActions.EditMailinglist,
  ) {
    const response = await this.apiService
      .putApiV1AdminMailinglistResponse(action.mailinglist)
      .toPromise();
    if (response.status === 200) {
      dispatch(new AdminActions.CloseModal());
    }
    dispatch(new AdminActions.GetAdminMailinglists());
  }

  @Action(AdminActions.AddWorkgroup)
  public async addWorkgroup(
    { dispatch }: StateContext<AdminStateModel>,
    action: AdminActions.AddWorkgroup,
  ) {
    const response = await this.apiService
      .postApiV1AdminWorkgroupResponse(action.workgroup)
      .toPromise();
    if (response.status === 201) {
      dispatch(new AdminActions.CloseModal());
      dispatch(new AdminActions.GetAllWorkgroups());
    }
  }

  @Action(AdminActions.CloseModal)
  public async closeModal({
    getState,
    patchState,
    dispatch,
  }: StateContext<AdminStateModel>) {
    getState().dialog?.close();
    patchState({ dialog: undefined });
    dispatch([
      new ResetForm({ path: 'admin.mailConfigForm' }),
      new ResetForm({ path: 'admin.workgroupConfigForm' }),
    ]);
  }

  @Action(AdminActions.GetAllWorkgroups)
  public async getAllWorkgroups({
    patchState,
    dispatch,
  }: StateContext<AdminStateModel>) {
    this.apiService
      .getApiV1AdminWorkgroupsResponse()
      .toPromise()
      .then(response => {
        if (response.status === 200) {
          patchState({
            workgroups: response.body,
          });
        }
      });
    dispatch(new WorkgroupActions.GetWorkgroups());
  }

  @Action(AdminActions.OpenEditWorkgroupDialog)
  public async openEditWorkgroupDialog(
    { dispatch, getState, patchState }: StateContext<AdminStateModel>,
    action: AdminActions.OpenEditWorkgroupDialog,
  ) {
    dispatch(new AdminActions.GetPossibleGroups())
      .pipe(first())
      .subscribe(() => {
        this.ngZone.run(() => {
          patchState({
            workgroupModalEditing: action.workgroup,
            workgroupModalState: ModalState.Edit,
            dialog: this.dialog.open(WorkgroupConfigDialogComponent, {
              width: '40em',
              maxWidth: '100%',
              maxHeight: '100vh',
              disableClose: true,
            }),
            workgroupConfigForm: {
              ...getState().mailConfigForm,
              model: {
                name: action.workgroup.name,
                matterMostLink: action.workgroup.matterMostLink,
                allowJoinLdapGroup: action.workgroup.allowJoinLdapGroup,
                mail: action.workgroup.mail,
                excludeMailinglistLdapGroup:
                  action.workgroup.excludeMailinglistLdapGroup,
                memberLdapGroup: action.workgroup.memberLdapGroup,
                speakerLdapGroup: action.workgroup.speakerLdapGroup,
              },
            },
          });
        });
      });
  }

  @Action(AdminActions.EditWorkgroup)
  public async editWorkgroup(
    { dispatch }: StateContext<AdminStateModel>,
    action: AdminActions.EditWorkgroup,
  ) {
    const response = await this.apiService
      .putApiV1AdminWorkgroupResponse(action.workgroup)
      .toPromise();
    if (response.status === 200) {
      dispatch(new AdminActions.CloseModal());
    }
    dispatch(new AdminActions.GetAllWorkgroups());
  }

  @Action(AdminActions.DeleteWorkgroup)
  public async deleteWorkgroup(
    { dispatch }: StateContext<AdminStateModel>,
    action: AdminActions.DeleteWorkgroup,
  ) {
    const response = await this.apiService
      .deleteApiV1AdminWorkgroupWgIdResponse(action.workgroup.uuid!)
      .toPromise();
    if (response.status === 200) {
      dispatch(new AdminActions.CloseModal());
    }
    dispatch(new AdminActions.GetAllWorkgroups());
  }

  @Action(AdminActions.AddSpeakerToWorkgroup)
  public async addSpeakerToWorkgroup(
    { dispatch }: StateContext<AdminStateModel>,
    action: AdminActions.AddSpeakerToWorkgroup,
  ) {
    const response = await this.apiService
      .postApiV1AdminWorkgroupWgIdSpeakerUserIdResponse({
        wgId: action.workgroupId,
        userId: action.userId,
      })
      .toPromise();
    dispatch(new WorkgroupActions.GetMembers(action.workgroupId));
  }

  @Action(AdminActions.RemoveSpeakerFromWorkgroup)
  public async removeSpeakerToWorkgroup(
    { dispatch }: StateContext<AdminStateModel>,
    action: AdminActions.RemoveSpeakerFromWorkgroup,
  ) {
    const response = await this.apiService
      .deleteApiV1AdminWorkgroupWgIdSpeakerUserIdResponse({
        wgId: action.workgroupId,
        userId: action.userId,
      })
      .toPromise();
    dispatch(new WorkgroupActions.GetMembers(action.workgroupId));
  }
}
