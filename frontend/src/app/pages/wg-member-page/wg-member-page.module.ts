import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonImportsModule } from 'src/app/common-imports/common-imports.module';
import { ComponentsModule } from 'src/app/components/components.module';
import { WgMemberPageComponent } from './wg-member-page.component';
@NgModule({
  declarations: [WgMemberPageComponent],
  imports: [CommonModule, CommonImportsModule, ComponentsModule],
})
export class WgMemberPageModule {}
