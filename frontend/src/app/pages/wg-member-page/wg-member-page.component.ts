import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserWorkgroupUserDto } from 'src/app/api/models';
import { WorkgroupActions } from './_store/workgroups.actions';
import { WorkgroupsStateModel } from './_store/workgroups.state';

@Component({
  selector: 'app-wg-member-page',
  templateUrl: './wg-member-page.component.html',
  styleUrls: ['./wg-member-page.component.scss'],
})
export class WgMemberPageComponent {
  @Select(
    (state: { workgroups: WorkgroupsStateModel }) =>
      state.workgroups.workgroups,
  )
  public workgroups$!: Observable<UserWorkgroupUserDto[]>;

  public validWorkgroups$?: Observable<UserWorkgroupUserDto[]>;

  constructor(private readonly store: Store) {}

  ngOnInit() {
    this.store.dispatch(new WorkgroupActions.GetWorkgroups());
    this.validWorkgroups$ = this.workgroups$.pipe(
      map(workgroups => workgroups?.filter(wg => wg.valid)),
    );
  }
}
