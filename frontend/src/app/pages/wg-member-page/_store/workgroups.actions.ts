import { UserWorkgroupUserDto } from 'src/app/api/models';

export namespace WorkgroupActions {
  export class GetWorkgroups {
    public static readonly type = '[Workgroup] GetWorkgroups';
  }

  export class JoinWorkgroup {
    public static readonly type = '[Workgroup] JoinWorkgroup';
    constructor(public wgId: string) {}
  }

  export class LeaveWorkgroup {
    public static readonly type = '[Workgroup] LeaveWorkgroup';
    constructor(public wgId: string) {}
  }

  export class JoinWorkgroupMailinglist {
    public static readonly type = '[Workgroup] JoinWorkgroupMailinglist';
    constructor(public wgId: string) {}
  }

  export class LeaveWorkgroupMailinglist {
    public static readonly type = '[Workgroup] LeaveWorkgroupMailinglist';
    constructor(public wgId: string) {}
  }

  export class OpenMembersDialog {
    public static readonly type = '[Workgroup] OpenMembersDialog';
    constructor(public wgId: string) {}
  }

  export class CloseModal {
    public static readonly type = '[Workgroup] CloseModal';
  }

  export class GetMembers {
    public static readonly type = '[Workgroup] GetMembers';
    constructor(public wgId: string) {}
  }

  export class RemoveMember {
    public static readonly type = '[Workgroup] RemoveMember';
    constructor(public wgId: string, public userId: string) {}
  }

  export class AddMember {
    public static readonly type = '[Workgroup] AddMember';
    constructor(public wgId: string, public userEmail: string) {}
  }
}
