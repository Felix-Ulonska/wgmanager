import { Injectable, NgZone } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Action, State, StateContext } from '@ngxs/store';
import { dispatch } from 'rxjs/internal/observable/pairs';
import { UserWgMemberDto, UserWorkgroupUserDto } from 'src/app/api/models';
import { ApiService } from 'src/app/api/services';
import { MemberListComponent } from 'src/app/components/member-list/member-list.component';
import { WorkgroupActions } from './workgroups.actions';

export interface WorkgroupsStateModel {
  workgroups: UserWorkgroupUserDto[];
  dialog?: MatDialogRef<any>;
  workgroupMembers: Record<string, UserWgMemberDto[]>;
  currentWorkgroupForMembersModalId: string;
}

@State({
  name: 'workgroups',
  defaults: {},
})
@Injectable()
export class WorkgroupsState {
  constructor(
    private readonly api: ApiService,
    private readonly dialog: MatDialog,
    private readonly ngZone: NgZone,
  ) {}

  @Action(WorkgroupActions.GetWorkgroups)
  public async getWorkgroups({
    patchState,
  }: StateContext<WorkgroupsStateModel>) {
    const response = await this.api
      .getApiV1UserWorkgroupsResponse()
      .toPromise();
    if (response.status === 200) {
      patchState({
        workgroups: response.body,
      });
    }
  }

  @Action(WorkgroupActions.LeaveWorkgroup)
  public async leaveWorkgroup(
    { dispatch }: StateContext<WorkgroupsStateModel>,
    action: WorkgroupActions.LeaveWorkgroup,
  ) {
    const response = await this.api
      .postApiV1UserWorkgroupUuidLeaveResponse(action.wgId)
      .toPromise();
    if (response.status === 200) {
      dispatch(new WorkgroupActions.GetWorkgroups());
    }
  }

  @Action(WorkgroupActions.JoinWorkgroup)
  public async joinWorkgroup(
    { dispatch }: StateContext<WorkgroupsStateModel>,
    action: WorkgroupActions.LeaveWorkgroup,
  ) {
    const response = await this.api
      .postApiV1UserWorkgroupUuidJoinResponse(action.wgId)
      .toPromise();
    if (response.ok) {
      dispatch(new WorkgroupActions.GetWorkgroups());
    }
  }

  @Action(WorkgroupActions.LeaveWorkgroupMailinglist)
  public async leaveWorkgroupMailinglist(
    { dispatch }: StateContext<WorkgroupsStateModel>,
    action: WorkgroupActions.LeaveWorkgroupMailinglist,
  ) {
    const response = await this.api
      .postApiV1UserWorkgroupUuidLeaveMailinglistResponse(action.wgId)
      .toPromise();
    console.log(response);
    if (response.ok) {
      console.log('GetWorkgroups');
      dispatch(new WorkgroupActions.GetWorkgroups());
    }
  }

  @Action(WorkgroupActions.JoinWorkgroupMailinglist)
  public async joinWorkgroupMailinglist(
    { dispatch }: StateContext<WorkgroupsStateModel>,
    action: WorkgroupActions.JoinWorkgroupMailinglist,
  ) {
    const response = await this.api
      .postApiV1UserWorkgroupUuidJoinMailinglistResponse(action.wgId)
      .toPromise();
    console.log(response);
    if (response.ok) {
      console.log('GetWorkgroups');
      dispatch(new WorkgroupActions.GetWorkgroups());
    }
  }

  @Action(WorkgroupActions.OpenMembersDialog)
  public async openMembersDialog(
    { patchState, dispatch }: StateContext<WorkgroupsStateModel>,
    action: WorkgroupActions.OpenMembersDialog,
  ) {
    dispatch(new WorkgroupActions.GetMembers(action.wgId));
    patchState({
      currentWorkgroupForMembersModalId: action.wgId,
      dialog: this.dialog.open(MemberListComponent, {
        maxWidth: '100%',
        maxHeight: '100vh',
      }),
    });
  }

  @Action(WorkgroupActions.CloseModal)
  public async closeModal({
    patchState,
    getState,
  }: StateContext<WorkgroupsStateModel>) {
    this.ngZone.run(() => {
      getState().dialog?.close();
    });
    patchState({
      currentWorkgroupForMembersModalId: undefined,
      dialog: undefined,
    });
  }

  @Action(WorkgroupActions.GetMembers)
  public async GetMembers(
    { patchState, getState }: StateContext<WorkgroupsStateModel>,
    action: WorkgroupActions.GetMembers,
  ) {
    const response = await this.api
      .getApiV1UserWorkgroupUuidMembersResponse(action.wgId)
      .toPromise();
    if (response.status === 200) {
      const newWgMembers = { ...getState().workgroupMembers };
      newWgMembers[action.wgId] = response.body;
      patchState({
        workgroupMembers: newWgMembers,
      });
    }
  }

  @Action(WorkgroupActions.RemoveMember)
  public async RemoveMember(
    { dispatch }: StateContext<WorkgroupsStateModel>,
    action: WorkgroupActions.RemoveMember,
  ) {
    const response = await this.api
      .deleteApiV1SpeakerWorkgroupUuidMembersUserIdResponse({
        uuid: action.wgId,
        userId: action.userId,
      })
      .toPromise();
    dispatch(new WorkgroupActions.GetMembers(action.wgId));
  }

  @Action(WorkgroupActions.AddMember)
  public async AddMember(
    { dispatch }: StateContext<WorkgroupsStateModel>,
    action: WorkgroupActions.AddMember,
  ) {
    const response = await this.api
      .postApiV1SpeakerWorkgroupUuidMembersUserMailResponse({
        uuid: action.wgId,
        userMail: action.userEmail,
      })
      .toPromise();
    dispatch(new WorkgroupActions.GetMembers(action.wgId));
  }
}
