import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler, Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { UiActions } from '../_store/ui.actions';

@Injectable()
export class CustomErrorHandler implements ErrorHandler {
  constructor(private store: Store) {}

  handleError(error: any) {
    if (error instanceof HttpErrorResponse) {
      this.store.dispatch(new UiActions.ReceivedHTTPError(error));
    }

    throw error;
  }
}
