import { Injectable } from '@angular/core';
import { Action, NgxsOnInit, State, StateContext } from '@ngxs/store';
import { KeycloakEventType, KeycloakService } from 'keycloak-angular';
import { filter } from 'rxjs/operators';
import { ApiService } from '../api/services';
import { AuthActions } from './auth.actions';
import jwt_decode from 'jwt-decode';

export interface AuthStateModel {
  isAdmin: boolean;
  emailVerified: boolean;
}

@State({
  name: 'auth',
  defaults: {
    emailVerified: false,
    isAdmin: false,
  },
})
@Injectable()
export class AuthState implements NgxsOnInit {
  constructor(private keycloak: KeycloakService, private api: ApiService) {}

  ngxsOnInit(ctx?: StateContext<AuthStateModel>) {
    this.keycloak.keycloakEvents$
      .pipe(
        filter(
          kEvent =>
            kEvent.type == KeycloakEventType.OnAuthSuccess ||
            kEvent.type == KeycloakEventType.OnAuthRefreshSuccess,
        ),
      )
      .subscribe(kevent => {
        ctx?.dispatch(new AuthActions.IsAdminRefresh());
        ctx?.dispatch(new AuthActions.RefreshEmailVerifyStatus());
      });
  }

  @Action(AuthActions.IsAdminRefresh)
  public refreshAdminStatus({ patchState }: StateContext<AuthStateModel>) {
    this.api.getApiV1AdminIsAdminResponse().subscribe(
      response => {
        patchState({ isAdmin: response.ok });
      },
      error => {
        patchState({ isAdmin: false });
      },
    );
  }

  @Action(AuthActions.RefreshEmailVerifyStatus)
  public async refreshEmailVerifyStatus({
    patchState,
  }: StateContext<AuthStateModel>) {
    patchState({
      emailVerified: (jwt_decode(await this.keycloak.getToken()) as any)
        .email_verified,
    });
  }
}
