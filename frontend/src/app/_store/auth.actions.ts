export namespace AuthActions {
  export class IsAdminRefresh {
    public static readonly type = '[AuthActions] IsAdminRefresh';
  }

  export class RefreshEmailVerifyStatus {
    public static readonly type = '[AuthActions] RefreshEmailVerifyStatus';
  }
}
