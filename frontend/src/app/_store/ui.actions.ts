import { HttpErrorResponse, HttpResponse } from '@angular/common/http';

export namespace UiActions {
  export class Resize {
    public static readonly type = '[UI] Resize';
    constructor(public newSize: { height: number; width: number }) {}
  }

  export class OpenSidenav {
    public static readonly type = '[UI] OpenSidenav';
  }

  export class CloseSidenav {
    public static readonly type = '[UI] CloseSidenav';
  }

  export class ToggleSidenav {
    public static readonly type = '[UI] ToggleSidenav';
  }

  export class ReceivedHTTPError {
    public static readonly type = '[UI] ReceivedHTTPError';
    constructor(public error: HttpErrorResponse) {}
  }

  export class SetDarkMode {
    public static readonly type = '[UI] SetDarkMode';
    constructor(public isDarkMode: boolean) {}
  }
}
