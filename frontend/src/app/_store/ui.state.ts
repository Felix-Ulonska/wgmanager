import { HostListener, Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Action, State, StateContext, Store } from '@ngxs/store';
import { UiActions } from './ui.actions';

export interface UiStateModel {
  height: number;
  width: number;
  mobileDesign: boolean;
  isSidenavOpened: boolean;
  darkMode: boolean;
}

@State({
  name: 'ui',
  defaults: {
    mobileDesign: window.innerWidth < 800,
    isSidenavOpened: window.innerWidth >= 800,
    width: window.innerWidth,
    height: window.innerHeight,
    darkMode: false,
  },
})
@Injectable()
export class UiState {
  constructor(private matSnackBar: MatSnackBar) {}

  @Action(UiActions.Resize)
  public resize(
    { patchState, getState, dispatch }: StateContext<UiStateModel>,
    action: UiActions.Resize,
  ) {
    if (action.newSize.width < 800 && getState().width >= 800) {
      dispatch(UiActions.CloseSidenav);
    } else if (action.newSize.width >= 800 && getState().width < 800) {
      dispatch(UiActions.OpenSidenav);
    }
    patchState({
      ...action.newSize,
      mobileDesign: action.newSize.width < 800,
    });
  }

  @Action(UiActions.CloseSidenav)
  public closeSidenav({ patchState }: StateContext<UiStateModel>) {
    patchState({ isSidenavOpened: false });
  }

  @Action(UiActions.OpenSidenav)
  public openSidenav({ patchState }: StateContext<UiStateModel>) {
    patchState({ isSidenavOpened: true });
  }

  @Action(UiActions.ToggleSidenav)
  public toggleSidenav({ patchState, getState }: StateContext<UiStateModel>) {
    patchState({ isSidenavOpened: !getState().isSidenavOpened });
  }

  @Action(UiActions.ReceivedHTTPError)
  public showError(
    {}: StateContext<UiStateModel>,
    action: UiActions.ReceivedHTTPError,
  ) {
    const errorTxt = `Die Anfrage war nicht erfolgreich. Fehlercode: ${action.error.status}`;
    this.matSnackBar.open(errorTxt, undefined, { duration: 5000 });
  }

  @Action(UiActions.SetDarkMode)
  public setDarkMode(
    { patchState }: StateContext<UiStateModel>,
    action: UiActions.SetDarkMode,
  ) {
    patchState({ darkMode: action.isDarkMode });
  }
}
