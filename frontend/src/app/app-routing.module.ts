import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuardGuard } from './guard/admin-guard.guard';
import { AdminPageComponent } from './pages/admin-page/admin-page.component';
import { AdminPageModule } from './pages/admin-page/admin-page.module';
import { MailinglistPageComponent } from './pages/mailinglist-page/mailinglist-page.component';
import { MailinglistPageModule } from './pages/mailinglist-page/mailinglist-page.module';
import { WgMemberPageComponent } from './pages/wg-member-page/wg-member-page.component';
import { WgMemberPageModule } from './pages/wg-member-page/wg-member-page.module';

const routes: Routes = [
  { path: '', redirectTo: '/mailinglists', pathMatch: 'full' },
  { path: 'mailinglists', component: MailinglistPageComponent },
  {
    path: 'admin',
    component: AdminPageComponent,
    canActivate: [AdminGuardGuard],
  },
  { path: 'memberships', component: WgMemberPageComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    MailinglistPageModule,
    AdminPageModule,
    WgMemberPageModule,
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
