import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MailinglistCardComponent } from './mailinglist-card.component';

describe('MailinglistCardComponent', () => {
  let component: MailinglistCardComponent;
  let fixture: ComponentFixture<MailinglistCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MailinglistCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MailinglistCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
