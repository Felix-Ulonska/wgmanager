import { Component, Input, OnInit } from '@angular/core';
import { SelectSnapshot } from '@ngxs-labs/select-snapshot';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import {
  ModelsLdapGroup,
  ModelsMailinglist,
  UserMailinglistUserDto,
} from 'src/app/api/models';
import { AdminActions } from 'src/app/pages/admin-page/_store/admin.actions';
import { AdminStateModel } from 'src/app/pages/admin-page/_store/admin.state';
import { MailinglistActions } from 'src/app/pages/mailinglist-page/_store/mailinglists.actions';
import { ConfirmDialogActions } from '../confirm-dialog/_state/confirm-dialog.action';

@Component({
  selector: 'app-mailinglist-card',
  templateUrl: './mailinglist-card.component.html',
  styleUrls: ['./mailinglist-card.component.scss'],
})
export class MailinglistCardComponent {
  @SelectSnapshot(
    (state: { admin: AdminStateModel }) => state.admin.mailinglists,
  )
  public adminMailinglists!: ModelsMailinglist[];

  @SelectSnapshot(
    (state: { admin: AdminStateModel }) => state.admin.possibleGroups,
  )
  public ldapGroups!: ModelsLdapGroup[];

  @Input()
  public adminView = false;

  @Input()
  public mailinglist!: UserMailinglistUserDto;

  public get adminMl() {
    return this.adminMailinglists?.find(
      ml => this.mailinglist.uuid === ml.uuid,
    );
  }

  public convertUuidToGroupname(id: string | undefined): string {
    if (id) {
      return this.ldapGroups.find(group => group.id === id)?.name ?? '';
    }
    return '';
  }

  constructor(private store: Store) {}

  public join(ml: UserMailinglistUserDto) {
    this.store.dispatch(
      new ConfirmDialogActions.Open({
        title: ml.name,
        body: `Willst du der Mailingliste ${ml.name} beitreten?`,
        actionOnConfirm: new MailinglistActions.JoinMailinglist(ml),
      }),
    );
  }

  public leave(ml: UserMailinglistUserDto) {
    this.store.dispatch(
      new ConfirmDialogActions.Open({
        title: ml.name,
        body: `Willst du der Mailingliste ${ml.name} verlassen?`,
        actionOnConfirm: new MailinglistActions.LeaveMailinglist(ml),
      }),
    );
  }

  public editMailinglist(mailinglist: ModelsMailinglist) {
    this.store.dispatch(new AdminActions.OpenEditMailinglistModal(mailinglist));
  }
}
