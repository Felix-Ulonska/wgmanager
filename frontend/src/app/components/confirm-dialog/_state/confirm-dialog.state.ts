import { Injectable, NgZone } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Action, State, StateContext, StateToken } from '@ngxs/store';
import { ConfirmDialogComponent } from '../confirm-dialog.component';
import { ConfirmDialogActions } from './confirm-dialog.action';

export interface ConfirmDialogStateModel {
  dialog?: MatDialogRef<any>;
  actionToDispatchOnConfirm?: any;
  body?: string;
  title?: string;
}

export const CONFIRM_DIALOG_STATE_TOKEN = new StateToken<ConfirmDialogState>(
  'confirmDialog',
);

@State({
  name: CONFIRM_DIALOG_STATE_TOKEN,
})
@Injectable()
export class ConfirmDialogState {
  constructor(
    private readonly dialog: MatDialog,
    private readonly ngZone: NgZone,
  ) {}

  @Action(ConfirmDialogActions.Open)
  public confirmDialogOpen(
    { patchState }: StateContext<ConfirmDialogStateModel>,
    action: ConfirmDialogActions.Open,
  ) {
    patchState({
      dialog: this.dialog.open(ConfirmDialogComponent),
      actionToDispatchOnConfirm: action.opts.actionOnConfirm,
      body: action.opts.body,
      title: action.opts.title,
    });
  }

  @Action(ConfirmDialogActions.Confirm)
  public confirm({
    dispatch,
    getState,
  }: StateContext<ConfirmDialogStateModel>) {
    dispatch(ConfirmDialogActions.Close);
    dispatch(getState().actionToDispatchOnConfirm);
  }

  @Action(ConfirmDialogActions.Close)
  public close({ setState, getState }: StateContext<ConfirmDialogStateModel>) {
    this.ngZone.run(() => {
      getState().dialog?.close();
      setState({});
    });
  }
}
