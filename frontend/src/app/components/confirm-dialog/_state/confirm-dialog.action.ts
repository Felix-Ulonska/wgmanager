export namespace ConfirmDialogActions {
  export class Open {
    public static readonly type = '[ConfirmDialogActions] openDialog';
    constructor(
      public opts: {
        actionOnConfirm?: any;
        body: string;
        title: string;
      },
    ) {}
  }

  export class Confirm {
    public static readonly type = '[ConfirmDialogActions] confirm';
  }

  export class Close {
    public static readonly type = '[ConfirmDialogActions] close';
  }
}
