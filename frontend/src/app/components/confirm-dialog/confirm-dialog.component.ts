import { Component, OnInit } from '@angular/core';
import { SelectSnapshot } from '@ngxs-labs/select-snapshot';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ConfirmDialogActions } from './_state/confirm-dialog.action';
import { ConfirmDialogStateModel } from './_state/confirm-dialog.state';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss'],
})
export class ConfirmDialogComponent {
  @Select(
    (state: { confirmDialog: ConfirmDialogStateModel }) =>
      (state.confirmDialog as ConfirmDialogStateModel).body,
  )
  public body!: Observable<string>;

  @Select(
    (state: { confirmDialog: ConfirmDialogStateModel }) =>
      (state.confirmDialog as ConfirmDialogStateModel).title,
  )
  public title!: Observable<string>;

  constructor(private readonly store: Store) {}

  public confirm() {
    this.store.dispatch(new ConfirmDialogActions.Confirm());
  }

  public close() {
    this.store.dispatch(new ConfirmDialogActions.Close());
  }
}
