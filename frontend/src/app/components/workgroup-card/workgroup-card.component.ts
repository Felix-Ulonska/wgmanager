import { Component, Input } from '@angular/core';
import { SelectSnapshot } from '@ngxs-labs/select-snapshot';
import { Store } from '@ngxs/store';
import {
  ModelsLdapGroup,
  ModelsWorkgroup,
  UserWorkgroupUserDto,
} from 'src/app/api/models';
import { AdminActions } from 'src/app/pages/admin-page/_store/admin.actions';
import { AdminStateModel } from 'src/app/pages/admin-page/_store/admin.state';
import { WorkgroupActions } from 'src/app/pages/wg-member-page/_store/workgroups.actions';
import { ConfirmDialogActions } from '../confirm-dialog/_state/confirm-dialog.action';

@Component({
  selector: 'app-workgroup-card',
  templateUrl: './workgroup-card.component.html',
  styleUrls: ['./workgroup-card.component.scss'],
})
export class WorkgroupCardComponent {
  @Input()
  public workgroup!: UserWorkgroupUserDto;

  @Input()
  public adminView = false;

  @SelectSnapshot((state: { admin: AdminStateModel }) => state.admin.workgroups)
  public workgroupAdmin!: ModelsWorkgroup[];

  @SelectSnapshot(
    (state: { admin: AdminStateModel }) => state.admin.possibleGroups,
  )
  public ldapGroups!: ModelsLdapGroup[];

  public get adminWg() {
    return this.workgroupAdmin?.find(wg => this.workgroup?.uuid === wg.uuid);
  }

  public convertUuidToGroupname(id: string | undefined): string {
    if (id) {
      return this.ldapGroups.find(group => group.id === id)?.name ?? '';
    }
    return '';
  }

  // stored sepratly to make the toggle seem instant
  public isSubscribedToMailinglist = false;

  constructor(private store: Store) {}

  public join(wg: UserWorkgroupUserDto) {
    this.store.dispatch(
      new ConfirmDialogActions.Open({
        title: wg.name,
        body: `Willst du der Mailingliste ${wg.name} beitreten?`,
        actionOnConfirm: new WorkgroupActions.JoinWorkgroup(wg.uuid),
      }),
    );
  }

  public leave(wg: UserWorkgroupUserDto) {
    this.store.dispatch(
      new ConfirmDialogActions.Open({
        title: wg.name,
        body: `Willst du der Mailingliste ${wg.name} verlassen?`,
        actionOnConfirm: new WorkgroupActions.LeaveWorkgroup(wg.uuid),
      }),
    );
  }

  public openMembers(wg: UserWorkgroupUserDto) {
    this.store.dispatch(new WorkgroupActions.OpenMembersDialog(wg.uuid));
  }

  public edit(workgroup: ModelsWorkgroup) {
    this.store.dispatch(new AdminActions.OpenEditWorkgroupDialog(workgroup));
  }

  public subscribeToMailinglist() {
    this.store.dispatch(
      new WorkgroupActions.JoinWorkgroupMailinglist(this.workgroup.uuid),
    );
  }

  public unsubscribeMailinglist() {
    this.store.dispatch(
      new WorkgroupActions.LeaveWorkgroupMailinglist(this.workgroup.uuid),
    );
  }
}
