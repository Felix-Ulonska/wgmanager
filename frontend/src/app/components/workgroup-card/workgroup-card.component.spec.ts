import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkgroupCardComponent } from './workgroup-card.component';

describe('WorkgroupCardComponent', () => {
  let component: WorkgroupCardComponent;
  let fixture: ComponentFixture<WorkgroupCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkgroupCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkgroupCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
