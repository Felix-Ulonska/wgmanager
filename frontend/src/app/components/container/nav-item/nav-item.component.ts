import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { Router } from '@angular/router';
import { Select } from '@ngxs/store';
import { Observable, Observer } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BaseComponent } from '../../base/base.component';

@Component({
  selector: 'app-nav-item',
  templateUrl: './nav-item.component.html',
  styleUrls: ['./nav-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavItemComponent extends BaseComponent implements OnInit {
  @Input()
  public title: string = '';

  @Input()
  public link: string = '';

  public selected = false;

  constructor(private router: Router) {
    super();
  }

  @Select(
    (state: { router: { state: { url: any } } }) => state.router.state.url,
  )
  url$!: Observable<string>;

  public ngOnInit(): void {
    this.url$.pipe(takeUntil(this.onDestory$)).subscribe((url: string) => {
      this.selected = url.includes(this.link);
    });
  }

  public route() {
    this.router.navigate([this.link]);
  }
}
