import { Component, Input, OnInit } from '@angular/core';
import { SelectMultipleControlValueAccessor } from '@angular/forms';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { ViewSelectSnapshot } from '@ngxs-labs/select-snapshot';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthStateModel } from 'src/app/_store/auth.state';
import { UiActions } from 'src/app/_store/ui.actions';
import { UiStateModel } from 'src/app/_store/ui.state';
import { BaseComponent } from '../base/base.component';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss'],
})
export class ContainerComponent extends BaseComponent {
  @Input()
  public title = '';

  @Select((store: { ui: UiStateModel }) => store.ui.isSidenavOpened)
  public isSidenavOpened$!: Observable<boolean>;

  @Select((store: { ui: UiStateModel }) => store.ui.mobileDesign)
  public mobileDesign$!: Observable<boolean>;

  @Select((state: { ui: UiStateModel }) => state.ui.darkMode)
  darkMode$!: Observable<boolean>;

  @Select((state: { auth: AuthStateModel }) => state.auth.isAdmin)
  isAdmin$!: Observable<boolean>;

  darkModeToggle!: boolean;

  constructor(private readonly store: Store) {
    super();
    this.darkModeToggle = (this.store.snapshot().ui as UiStateModel).darkMode;
  }

  ngOnInit() {
    this.darkMode$.pipe(takeUntil(this.onDestory$)).subscribe(darkMode => {
      this.darkModeToggle = darkMode;
    });
  }

  public sideToogle() {
    this.store.dispatch(new UiActions.ToggleSidenav());
  }

  public changedDarkMode() {
    this.store.dispatch(new UiActions.SetDarkMode(!this.darkModeToggle));
  }
}
