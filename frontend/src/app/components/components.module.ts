import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseComponent } from './base/base.component';
import { ContainerComponent } from './container/container.component';
import { CommonImportsModule } from '../common-imports/common-imports.module';
import { NavItemComponent } from './container/nav-item/nav-item.component';
import { MemberListComponent } from './member-list/member-list.component';
import { ChipComponent } from './chip/chip.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { ButtonRowFormComponent } from './button-row-form/button-row-form.component';
import { WorkgroupCardComponent } from './workgroup-card/workgroup-card.component';
import { MailinglistCardComponent } from './mailinglist-card/mailinglist-card.component';

@NgModule({
  declarations: [
    ContainerComponent,
    BaseComponent,
    NavItemComponent,
    MemberListComponent,
    ChipComponent,
    ConfirmDialogComponent,
    ButtonRowFormComponent,
    WorkgroupCardComponent,
    MailinglistCardComponent,
  ],
  imports: [CommonModule, CommonImportsModule],
  exports: [
    ContainerComponent,
    BaseComponent,
    ChipComponent,
    ConfirmDialogComponent,
    ButtonRowFormComponent,
    MailinglistCardComponent,
    WorkgroupCardComponent,
  ],
})
export class ComponentsModule {}
