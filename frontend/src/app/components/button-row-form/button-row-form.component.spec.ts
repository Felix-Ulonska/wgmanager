import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonRowFormComponent } from './button-row-form.component';

describe('ButtonRowFormComponent', () => {
  let component: ButtonRowFormComponent;
  let fixture: ComponentFixture<ButtonRowFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonRowFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonRowFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
