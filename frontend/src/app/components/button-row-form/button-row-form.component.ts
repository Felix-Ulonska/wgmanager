import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-button-row-form',
  templateUrl: './button-row-form.component.html',
  styleUrls: ['./button-row-form.component.scss'],
})
export class ButtonRowFormComponent {
  @Output()
  public delete = new EventEmitter<void>();

  @Output()
  public submit = new EventEmitter<void>();

  @Output()
  public cancel = new EventEmitter<void>();

  @Input()
  public showDelete = false;

  @Input()
  public enableDeleteButton = false;

  @Input()
  public submitLabel = 'Bestätigen';

  @Input()
  public cancelLabel = 'Abbrechen';

  @Input()
  public deleteLabel = 'Löschen';
}
