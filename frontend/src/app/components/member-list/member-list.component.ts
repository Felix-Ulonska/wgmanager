import { DataSource } from '@angular/cdk/collections';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { SelectSnapshot } from '@ngxs-labs/select-snapshot';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { filter, first, takeUntil } from 'rxjs/operators';
import { UserWgMemberDto } from 'src/app/api/models';
import { AdminActions } from 'src/app/pages/admin-page/_store/admin.actions';
import { WorkgroupActions } from 'src/app/pages/wg-member-page/_store/workgroups.actions';
import { WorkgroupsStateModel } from 'src/app/pages/wg-member-page/_store/workgroups.state';
import { AuthStateModel } from 'src/app/_store/auth.state';
import { UiStateModel } from 'src/app/_store/ui.state';
import { BaseComponent } from '../base/base.component';

@Component({
  selector: 'app-member-list',
  templateUrl: './member-list.component.html',
  styleUrls: ['./member-list.component.scss'],
})
export class MemberListComponent extends BaseComponent implements OnInit {
  displayedColumns = ['name', 'mail', 'kick', 'toggleSpeaker'];

  public dataSource = new MatTableDataSource();

  public formAddUser = new FormGroup({
    email: new FormControl(null, [Validators.required, Validators.email]),
  });

  @Select(
    (state: any) => (state.workgroups as WorkgroupsStateModel).workgroupMembers,
  )
  workgroupMembers$!: Observable<Record<string, UserWgMemberDto[]>>;

  @Select((state: { auth: AuthStateModel }) => state.auth.isAdmin)
  public isAdmin$!: Observable<boolean>;

  @Select(
    (state: { workgroups: WorkgroupsStateModel }) =>
      state.workgroups.currentWorkgroupForMembersModalId,
  )
  public currWorkgroup$!: Observable<string>;

  public userEmailNotFound = false;

  constructor(private store: Store) {
    super();
  }

  async ngOnInit(): Promise<void> {
    this.workgroupMembers$
      .pipe(
        takeUntil(this.onDestory$),
        filter(members => !!members),
      )
      .subscribe(async members => {
        this.dataSource.data = members[await this.getWorkgroupId()];
      });
    const isAdmin = await this.isAdmin$.pipe(first()).toPromise();
    const wgUuid = await this.currWorkgroup$.pipe(first()).toPromise();
    const wg = this.store
      .selectSnapshot(
        (state: { workgroups: WorkgroupsStateModel }) =>
          state.workgroups.workgroups,
      )
      .find(async wg => wg.uuid === wgUuid);
    this.displayedColumns = ['name', 'mail'];
    if (wg?.isSpeaker || isAdmin) this.displayedColumns.push('kick');
    if (isAdmin) this.displayedColumns.push('toggleSpeaker');
  }

  private async getWorkgroupId(): Promise<string> {
    return await this.currWorkgroup$.pipe(first()).toPromise();
  }

  public async removeUser(userId: string) {
    this.store.dispatch(
      new WorkgroupActions.RemoveMember(await this.getWorkgroupId(), userId),
    );
  }

  public async submitAddUserForm() {
    this.formAddUser.get('email')!.setErrors(null);
    this.store
      .dispatch(
        new WorkgroupActions.AddMember(
          await this.getWorkgroupId(),
          this.formAddUser.get('email')!.value,
        ),
      )
      .subscribe(
        () => {
          this.formAddUser.get('email')!.setValue('');
          this.formAddUser.get('email')!.setErrors(null);
        },
        error => {
          if (error instanceof HttpErrorResponse) {
            if (error.status === 404) {
              console.log('ERROR');
              this.formAddUser.get('email')!.setErrors({ userNotFound: true });
              console.log(this.formAddUser);
            }
          }
        },
      );
  }

  public close() {
    this.store.dispatch(new WorkgroupActions.CloseModal());
  }

  public async addSpeaker(userId: string) {
    this.store.dispatch(
      new AdminActions.AddSpeakerToWorkgroup(
        await this.getWorkgroupId(),
        userId,
      ),
    );
  }
  public async removeSpeaker(userId: string) {
    this.store.dispatch(
      new AdminActions.RemoveSpeakerFromWorkgroup(
        await this.getWorkgroupId(),
        userId,
      ),
    );
  }
}
