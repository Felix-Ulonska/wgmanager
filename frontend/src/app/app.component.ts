import { HostListener, Renderer2 } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { KeycloakEventType, KeycloakService } from 'keycloak-angular';
import { Observable } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { ApiService } from './api/services';
import { BaseComponent } from './components/base/base.component';
import { AuthStateModel } from './_store/auth.state';
import { UiActions } from './_store/ui.actions';
import { UiState, UiStateModel } from './_store/ui.state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent extends BaseComponent implements OnInit {
  @Select((state: { ui: UiStateModel }) => state.ui.darkMode)
  darkMode$!: Observable<boolean>;

  @Select((state: { auth: AuthStateModel }) => state.auth.emailVerified)
  emailVerified$!: Observable<boolean>;

  constructor(private readonly renderer: Renderer2, private store: Store) {
    super();
  }
  ngOnInit(): void {
    this.darkMode$
      .pipe(takeUntil(this.onDestory$))
      .subscribe((darkMode: boolean) => {
        if (darkMode) {
          this.renderer.addClass(document.body, 'theme-alternate');
        } else {
          this.renderer.removeClass(document.body, 'theme-alternate');
        }
      });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.store.dispatch(
      new UiActions.Resize({
        width: event.target.innerWidth,
        height: event.target.innerHeight,
      }),
    );
  }
}
