import {
  getElem,
  loginAdmin,
  loginUsera,
  resetBackend,
} from 'cypress/utils/utils';

describe('Can Change Memberstatus of Workgroup', () => {
  beforeEach(() => {
    loginUsera();
    cy.visit('/');
    resetBackend();
    cy.wait(2000);
  });

  it('Can join Mailinglist', () => {
    // prevent weird timeout errors
    getElem('nav-member').click();
    getElem('membership-wg').then(val => {
      if (val[0].innerText === 'true') {
        getElem('leave-wg').click();
        cy.document().contains('false');
      }
      getElem('join-wg').click();
      cy.wait(100);
      cy.document().contains('true');
      cy.wait(100);
      getElem('leave-wg').click();
      cy.wait(100);
      cy.document().contains('false');
    });
  });
});
