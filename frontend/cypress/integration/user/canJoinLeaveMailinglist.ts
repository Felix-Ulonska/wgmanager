import {
  getElem,
  loginAdmin,
  loginUsera,
  resetBackend,
} from 'cypress/utils/utils';

describe.skip('Can Change Memberstatus of Mailinglist', () => {
  beforeEach(() => {
    loginUsera();
    cy.visit('/');
    resetBackend();
    // prevent weird timeout errors
    cy.wait(2000);
  });

  it('Can join Mailinglist', () => {
    /* ==== Generated with Cypress Studio ==== */
    getElem('ml-membership-status').then(val => {
      if (val[0].innerText.trim() === 'true') {
        getElem('leave-ml').click();
      }
      cy.wait(100);
      cy.document().contains('false');
      cy.wait(100);
      getElem('join-ml').click();
      cy.wait(100);
      getElem('ml-membership-status').contains('true');
      cy.wait(100);
      getElem('leave-ml').click();
      cy.wait(100);
      cy.document().contains('false');
    });
    /* ==== End Cypress Studio ==== */
  });
});
