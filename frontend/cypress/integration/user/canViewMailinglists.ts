import { getElem, loginAdmin, resetBackend } from 'cypress/utils/utils';

describe('Can View Mailinglist', () => {
  beforeEach(() => {
    resetBackend();
    loginAdmin();
  });

  it('Can edit Mailinglist', () => {
    cy.visit('/');
    getElem('nav-ml').click();

    cy.document().should('contain.text', 'testForEdit');
  });
});
