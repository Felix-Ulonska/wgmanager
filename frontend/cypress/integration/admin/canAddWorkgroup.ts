import { Interception } from 'cypress/types/net-stubbing';
import { getElem, loginAdmin, resetBackend } from 'cypress/utils/utils';

describe('Can Add Workgroup', () => {
  beforeEach(() => {
    resetBackend();
    loginAdmin();
    cy.visit('/');
    getElem('nav-admin').click();
  });
  it('Can Add Workgroup', () => {
    cy.intercept({
      method: 'POST',
      url: '/api/v1/admin/workgroup',
    }).as('apiCall');
    cy.get('[data-cy=addWgButton] > .mat-button-wrapper').click();
    cy.get('#mat-input-0').clear();
    cy.get('#mat-input-0').type('testName');
    cy.get('#mat-input-1').clear();
    cy.get('#mat-input-1').type('test@example.org');
    cy.get('#mat-input-2').clear();
    cy.get('#mat-input-2').type('matlink.example.org');
    cy.get('mat-select').eq(0).click();
    cy.get('mat-option').eq(0).click();
    //cy.get('mat-select').eq(0).type('{esc}');
    cy.get('mat-select').eq(1).click();
    cy.get('mat-option').eq(1).click();
    // cy.get('mat-select').eq(1).type('{esc}');
    cy.get('mat-select').eq(2).click();
    cy.get('mat-option').eq(2).click();
    // cy.get('mat-select').eq(2).type('{esc}');

    getElem('wg-submit').click();
    cy.wait('@apiCall').then((interception: Interception) => {
      assert.isNotNull(interception.response);
      if (interception.response) {
        assert.equal(interception.response.statusCode, 201);
        assert.isNotNull(interception.response.body);
      }
    });
    cy.document().contains('testName');
  });
});
