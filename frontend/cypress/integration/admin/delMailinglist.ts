import { getElem, loginAdmin, resetBackend } from 'cypress/utils/utils';

describe('Del Mailinglist', () => {
  beforeEach(() => {
    resetBackend();
    loginAdmin();
  });

  it('Can delete Mailinglist', async () => {
    cy.visit('/');
    getElem('nav-admin').click();
    cy.url().should('include', '/admin');
    cy.document().should('contain.text', 'testForEdit');
    cy.get('b').should('have.text', 'testForEdit');
    cy.get('b').should('be.visible');
    cy.get('[data-cy=edit-ml] > .mat-button-wrapper').click();
    cy.get('[data-cy=ml-delete] > .mat-button-wrapper').click();
    cy.document().should('not.contain.text', 'testForEdit');
  });
});
