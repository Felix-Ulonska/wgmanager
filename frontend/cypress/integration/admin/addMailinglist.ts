import { Interception } from 'cypress/types/net-stubbing';
import { getElem, loginAdmin, resetBackend } from 'cypress/utils/utils';

describe('Add Mailinglist', () => {
  beforeEach(() => {
    resetBackend();
    loginAdmin();
    cy.visit('/');
  });

  it('Can open Dialog', async () => {
    getElem('nav-ml').click();
    cy.url().should('include', '/mailinglists');
    getElem('nav-admin').click();
    getElem('addMlButton').click();
    getElem('ml-dialog-heading').should('be.visible');
    getElem('ml-input-name').should('be.visible');
    const newName = (Math.random() + 1).toString(36);
    getElem('ml-input-name').type(newName);
    cy.get('mat-select').first().click();
    cy.get('mat-option').first().click();
    cy.get('mat-select').first().type('{esc}');
    cy.get('mat-select').eq(1).click();
    cy.get('mat-option').first().click();
    cy.get('mat-select').eq(1).type('{esc}');
    getElem('ml-input-email').should('be.visible');
    getElem('ml-input-email').type('neue@ml.com');
    getElem('ml-submit').should('be.visible');
    cy.intercept({
      method: 'POST',
      url: '/api/v1/admin/mailinglists',
    }).as('apiCall');
    getElem('ml-submit').click();
    cy.wait('@apiCall').then((interception: Interception) => {
      assert.isNotNull(interception.response);
      if (interception.response) {
        assert.equal(interception.response.statusCode, 201);
        assert.isNotNull(interception.response.body);
      }
    });
    cy.contains(newName).should('not.be.null');
  });
});
