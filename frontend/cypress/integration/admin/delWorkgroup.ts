import { getElem, loginAdmin, resetBackend } from 'cypress/utils/utils';

describe('Del Mailinglist', () => {
  beforeEach(() => {
    resetBackend();
    loginAdmin();
  });

  it('Can delete Mailinglist', async () => {
    cy.visit('/');
    getElem('nav-admin').click();
    cy.url().should('include', '/admin');
    cy.document().should('contain.text', 'testGroup');
    cy.get('[data-cy=wg-edit]').click();
    cy.get('[data-cy=wg-delete] > .mat-button-wrapper').click();
    cy.document().should('not.contain.text', 'testGroup');
  });
});
