import { getElem, loginAdmin, resetBackend } from 'cypress/utils/utils';

describe('Can edit Mailinglist', () => {
  beforeEach(() => {
    resetBackend();
    loginAdmin();
    cy.visit('/');
  });

  it('Can edit Mailinglist', () => {
    getElem('nav-admin').click();
    getElem('edit-ml').click();
    cy.get('[data-cy=ml-input-name]').clear();
    cy.get('[data-cy=ml-input-name]').type('testForEditChange');
    cy.get('[data-cy=ml-input-email]').clear();
    cy.get('[data-cy=ml-input-email]').type('testanders@example.org');
    cy.get('[data-cy=ml-submit] > .mat-button-wrapper').click();
    cy.document().contains('testanders@example.org');
  });
});
