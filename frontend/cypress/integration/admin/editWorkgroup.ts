import { getElem, loginAdmin, resetBackend } from 'cypress/utils/utils';

describe('Can edit Workroup', () => {
  beforeEach(() => {
    resetBackend();
    loginAdmin();
    cy.visit('/');
    getElem('nav-admin').click();
  });
  it('can edit workgroup', () => {
    /* ==== Generated with Cypress Studio ==== */
    getElem('wg-edit').click();
    cy.get('#mat-input-0').clear();
    cy.get('#mat-input-0').type('testGroupEdited');
    getElem('wg-submit').click();
    cy.get(
      '.table-padding > .mat-table > tbody > .mat-row > .cdk-column-name > b',
    ).should('contain', 'testGroupEdited');
    /* ==== End Cypress Studio ==== */
  });
});
