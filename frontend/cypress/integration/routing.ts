import '../support/commands';
import { getElem, loginAdmin, resetBackend } from '../utils/utils';

beforeEach(() => {
  resetBackend();
  loginAdmin();
});

it('navigate works', () => {
  cy.visit('/');
  cy.url().should('include', '/mailinglists');
  getElem('nav-member').click();
  cy.url().should('include', '/memberships');
  getElem('nav-admin').click();
  cy.url().should('include', '/admin');
  getElem('nav-ml').click();
  cy.url().should('include', '/mailinglists');
});
