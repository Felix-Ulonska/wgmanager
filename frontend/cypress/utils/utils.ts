export function getElem(selector: string): Cypress.Chainable {
  return cy.get(`[data-cy=${selector}]`);
}

export function resetBackend(): Cypress.Chainable<Cypress.Response<any>> {
  return cy.request('POST', '/api/v1/testing/reset');
}

export function loginAdmin() {
  cy.logout({
    root: 'http://localhost:8080',
    realm: 'WgManager',
    redirect_uri: 'http://localhost:8000',
  });
  cy.login({
    root: 'http://localhost:8080',
    realm: 'WgManager',
    redirect_uri: 'http://localhost:8000',
    username: 'wgadminuser',
    password: 'pass123',
    client_id: 'WgManager',
  });
  cy.wait(2000);
}

export function loginUsera() {
  cy.logout({
    root: 'http://localhost:8080',
    realm: 'WgManager',
    redirect_uri: 'http://localhost:8000',
  });
  cy.login({
    root: 'http://localhost:8080',
    realm: 'WgManager',
    redirect_uri: 'http://localhost:8000',
    username: 'User1',
    password: 'pass123',
    client_id: 'WgManager',
  });
}
