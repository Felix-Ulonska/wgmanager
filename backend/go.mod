module gitlab.com/Felix-Ulonska/wgmanager

go 1.16

require (
	github.com/MicahParks/keyfunc v0.6.1
	github.com/Nerzal/gocloak/v8 v8.5.0
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/barkimedes/go-deepcopy v0.0.0-20200817023428-a044a1957ca4
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/emersion/go-sasl v0.0.0-20200509203442-7bfe0ed36a21
	github.com/emersion/go-smtp v0.15.0
	github.com/form3tech-oss/jwt-go v3.2.3+incompatible // indirect
	github.com/gin-contrib/gzip v0.0.1
	github.com/gin-contrib/static v0.0.1
	github.com/gin-gonic/contrib v0.0.0-20201101042839-6a891bf89f19 // indirect
	github.com/gin-gonic/gin v1.7.3
	github.com/go-openapi/jsonreference v0.19.6 // indirect
	github.com/go-openapi/spec v0.20.3 // indirect
	github.com/go-openapi/swag v0.19.15 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/google/uuid v1.3.0
	github.com/joho/godotenv v1.3.0
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mandrigin/gin-spa v0.0.0-20200212133200-790d0c0c7335
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.1
	github.com/swaggo/files v0.0.0-20190704085106-630677cd5c14
	github.com/swaggo/gin-swagger v1.3.1
	github.com/swaggo/swag v1.7.0
	golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d // indirect
	golang.org/x/sys v0.0.0-20210806184541-e5e7981a1069 // indirect
	golang.org/x/tools v0.1.5 // indirect
)
