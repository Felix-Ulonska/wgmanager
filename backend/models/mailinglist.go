package models

import (
	"github.com/Nerzal/gocloak/v8"
	"gitlab.com/Felix-Ulonska/wgmanager/utils/keycloak"
)

/// Mailinglist. User is member of mailinglist if the user is in the base
type Mailinglist struct {
	UUID                   string `json:"uuid"`
	Name                   string `json:"name" binding:"required"`
	Email                  string `json:"email" binding:"required"`
	BaseGroup              string `json:"baseGroup" binding:"required"`     // Members of this group will be added to mailinglist if ModifierGroupIsInclude=true else these users are added to group if inside ModifierGroup
	ModifierGroup          string `json:"modifierGroup" binding:"required"` // Members of this group will be added if modifierGroupIsInclude else these users will be removed
	ModifierGroupIsInclude bool   `json:"modifierGroupIsInclude"`
	Valid                  bool   `json:"valid"` /// Is true when all groups exist.
}

func (m Mailinglist) IsUserAllowedToJoin(userId string) (bool, error) {
	k := keycloak.GetKeycloak()

	user, err := k.GetUserById(userId)
	if err != nil {
		return false, err
	}
	isInInclude, err := k.IsMemeberOfGroup(userId, m.BaseGroup)
	if err != nil {
		return false, err
	}

	return isInInclude && *(user.EmailVerified), nil
}

func (m Mailinglist) IsUserInMailinglist(userId string) (bool, error) {
	k := keycloak.GetKeycloak()

	isInBaseGroup, err := k.IsMemeberOfGroup(userId, m.BaseGroup)
	if err != nil {
		return false, err
	}
	isInModifier, err := k.IsMemeberOfGroup(userId, m.ModifierGroup)
	if err != nil {
		return false, err
	}
	// First clause: ModifierGroupIsInclude and user isInModifier,  second clause modifierGroupIs Exclude, and user is not in group
	return (isInBaseGroup && isInModifier && m.ModifierGroupIsInclude) || (isInBaseGroup && !isInModifier && !m.ModifierGroupIsInclude), nil
}

func (m Mailinglist) Members() ([]*gocloak.User, error) {
	k := keycloak.GetKeycloak()
	includeGroup, err := k.MembersOfGroup(m.BaseGroup)
	if err != nil {
		return nil, err
	}
	excludeGroup, err := k.MembersOfGroup(m.ModifierGroup)
	if err != nil {
		return nil, err
	}
	members := make([]*gocloak.User, 0)
	for _, u := range includeGroup {
		included := !m.ModifierGroupIsInclude
		for _, u2 := range excludeGroup {
			if *u2.ID == *u.ID {
				included = m.ModifierGroupIsInclude
			}
		}
		if included {
			members = append(members, u)
		}
	}
	return members, nil
}

func (m Mailinglist) MailinglistMembers() ([]*gocloak.User, error) {
	return m.Members()
}

type NotAllowed struct{}

func (m NotAllowed) Error() string {
	return "Not Allowed"
}

func (m Mailinglist) Listaddress() string {
	return m.Email
}

// Adds user to Mailinglist
func (m Mailinglist) AddUser(userId string) error {
	k := keycloak.GetKeycloak()
	allowed, err := m.IsUserAllowedToJoin(userId)
	if err != nil {
		return err
	}
	if !allowed {
		return &NotAllowed{}
	}
	if m.ModifierGroupIsInclude {
		return k.AddUserToGroup(userId, m.ModifierGroup)
	} else {
		return k.DeleteUserFromGroup(userId, m.ModifierGroup)
	}
}

// Adds user to Mailinglist
func (m Mailinglist) RemoveUser(userId string) error {
	k := keycloak.GetKeycloak()
	allowed, err := m.IsUserAllowedToJoin(userId)
	if err != nil {
		return err
	}
	if !allowed {
		return &NotAllowed{}
	}
	if !m.ModifierGroupIsInclude {
		return k.AddUserToGroup(userId, m.ModifierGroup)
	} else {
		return k.DeleteUserFromGroup(userId, m.ModifierGroup)
	}
}

// Checks if Mailinglist is valid with groups
func (m *Mailinglist) RefreshValid(groups []*gocloak.Group) {
	modifierGroupExists := false
	includeGroupExist := false
	for _, g := range groups {
		if *g.ID == m.ModifierGroup {
			modifierGroupExists = true
		}
		if *g.ID == m.BaseGroup {
			includeGroupExist = true
		}
	}
	m.Valid = includeGroupExist && modifierGroupExists
}

func (m Mailinglist) IsValid() bool {
	return m.Valid
}
