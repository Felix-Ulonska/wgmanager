package models

type LdapGroup struct {
	Name string `json:"name"`
	Id   string `json:"id"`
	Path string `json:"path"`
}
