package models

type Config struct {
	Mailinglists []Mailinglist `json:"mailingslists"`
	Workgroups   []Workgroup   `json:"workgroups"`
}
