package models

import (
	"github.com/Nerzal/gocloak/v8"
	"github.com/pkg/errors"
	"gitlab.com/Felix-Ulonska/wgmanager/utils/keycloak"
	"gitlab.com/Felix-Ulonska/wgmanager/utils/mail"
)

type Workgroup struct {
	UUID                        string `json:"uuid" binding:"required,uuid"`
	AllowJoinLdapGroup          string `json:"allowJoinLdapGroup" binding:"required,uuid"`
	ExcludeMailinglistLdapGroup string `json:"excludeMailinglistLdapGroup" binding:"required,uuid"`
	Mail                        string `json:"mail" binding:"omitempty,email"`
	MatterMostLink              string `json:"matterMostLink" binding:"omitempty,url"`
	MemberLdapGroup             string `json:"memberLdapGroup" binding:"required,uuid"`
	Name                        string `json:"name" binding:"required,min=2,max=100" `
	SpeakerLdapGroup            string `json:"speakerLdapGroup" binding:"required,uuid"`
	Valid                       bool   `json:"valid" `
}

func (w Workgroup) IsUserAllowedToJoin(userId string) (bool, error) {
	k := keycloak.GetKeycloak()
	user, err := k.GetUserById(userId)
	if err != nil {
		return false, err
	}
	isMember, err := k.IsMemeberOfGroup(userId, w.AllowJoinLdapGroup)
	if err != nil {
		return false, err
	}
	return *user.EmailVerified && isMember, nil
}

func (w Workgroup) IsUserJoined(userId string) (bool, error) {
	k := keycloak.GetKeycloak()
	return k.IsMemeberOfGroup(userId, w.MemberLdapGroup)
}

func (w Workgroup) AddUser(userId string, ignoreAllowGroup bool) error {
	k := keycloak.GetKeycloak()
	isAllowed, err := w.IsUserAllowedToJoin(userId)
	if err != nil {
		return err
	}
	if !isAllowed && !ignoreAllowGroup {
		return errors.New("Not Allowed to join")
	}

	user, err := k.GetUserById(userId)
	if err != nil {
		return errors.New("Error getting user from Keycloak")
	}
	if !(*user.EmailVerified) {
		return errors.New("User is not verified")
	}

	go mail.GetSmtpConnection().SendUserAddedToGroupMail(userId, w.Name)
	speaker, err := w.Speaker()
	if err != nil {
		return err
	}
	go mail.GetSmtpConnection().SendSpeakerNewMemberNotification(userId, w.Name, speaker)
	return k.AddUserToGroup(userId, w.MemberLdapGroup)
}

func (w Workgroup) DeleteUser(userId string) error {
	k := keycloak.GetKeycloak()
	isMember, err := k.IsMemeberOfGroup(userId, w.MemberLdapGroup)
	if err != nil {
		return err
	}
	// TODO better handeling
	if !isMember {
		return errors.New("Is not member")
	}

	go mail.GetSmtpConnection().SendUserRemovedFromGroupMail(userId, w.Name)
	speaker, err := w.Speaker()
	go mail.GetSmtpConnection().SendSpeakerMemberRemovedNotification(userId, w.Name, speaker)
	for _, group := range []string{w.ExcludeMailinglistLdapGroup, w.SpeakerLdapGroup, w.MemberLdapGroup} {
		isMemberOfGroup, err := k.IsMemeberOfGroup((userId), group)
		if err != nil {
			return errors.Wrap(err, "Failed to check if member of Group")
		}
		if isMemberOfGroup {
			if err = k.DeleteUserFromGroup(userId, group); err != nil {
				return errors.Wrap(err, "Error removing user from Group")
			}
		}
	}
	return nil
}

func (w Workgroup) AddUserToMailinglist(userId string) error {
	k := keycloak.GetKeycloak()
	isMember, err := k.IsMemeberOfGroup(userId, w.MemberLdapGroup)
	if err != nil {
		return err
	}
	// TODO better handeling
	if !isMember {
		return errors.New("Is not member")
	}
	return k.DeleteUserFromGroup(userId, w.ExcludeMailinglistLdapGroup)
}

func (w Workgroup) RemoveUserFromMailinglist(userId string) error {
	k := keycloak.GetKeycloak()
	isMember, err := k.IsMemeberOfGroup(userId, w.MemberLdapGroup)
	if err != nil {
		return err
	}
	// TODO better handeling
	if !isMember {
		return errors.New("Is not member")
	}
	return k.AddUserToGroup(userId, w.ExcludeMailinglistLdapGroup)
}

func (w Workgroup) Members() ([]*gocloak.User, error) {
	k := keycloak.GetKeycloak()
	return k.MembersOfGroup(w.MemberLdapGroup)
}

func (w Workgroup) MailinglistMembers() ([]*gocloak.User, error) {
	k := keycloak.GetKeycloak()
	groupMembers, err := k.MembersOfGroup(w.MemberLdapGroup)
	if err != nil {
		return nil, err
	}
	excludeMembers, err := k.MembersOfGroup(w.ExcludeMailinglistLdapGroup)
	mailinglistMembers := make([]*gocloak.User, 0)
	for _, u := range groupMembers {
		excluded := false
		for _, u2 := range excludeMembers {
			if *u2.ID == *u.ID {
				excluded = true
			}
		}
		if !excluded {
			mailinglistMembers = append(mailinglistMembers, u)
		}
	}
	return mailinglistMembers, nil
}

func (w Workgroup) IsUserSubscribedToMailinglist(userId string) (bool, error) {
	k := keycloak.GetKeycloak()
	member, err := k.IsMemeberOfGroup(userId, w.MemberLdapGroup)
	if err != nil {
		return false, err
	}
	excludedML, err := k.IsMemeberOfGroup(userId, w.ExcludeMailinglistLdapGroup)
	if err != nil {
		return false, err
	}
	return member && !excludedML, nil
}

func (w Workgroup) Speaker() ([]*gocloak.User, error) {
	k := keycloak.GetKeycloak()
	return k.MembersOfGroup(w.SpeakerLdapGroup)
}

func (w Workgroup) AddSpeaker(userId string) error {
	k := keycloak.GetKeycloak()
	return k.AddUserToGroup(userId, w.SpeakerLdapGroup)
}

func (w Workgroup) RemoveSpeaker(userId string) error {
	k := keycloak.GetKeycloak()
	return k.DeleteUserFromGroup(userId, w.SpeakerLdapGroup)
}

func (w Workgroup) IsSpeaker(userId string) (bool, error) {
	k := keycloak.GetKeycloak()
	return k.IsMemeberOfGroup(userId, w.SpeakerLdapGroup)

}

func (w Workgroup) Listaddress() string {
	return w.Mail
}

// Checks if Workgroup is valid with groups in keycloak
func (w *Workgroup) RefreshValid(groups []*gocloak.Group) {
	excludeGroupExist := false
	allowGroupExist := false
	memberLdapGroupExist := false
	speakerLdapGroupExist := false

	for _, g := range groups {
		if *g.ID == w.ExcludeMailinglistLdapGroup {
			excludeGroupExist = true
		}
		if *g.ID == w.AllowJoinLdapGroup {
			allowGroupExist = true
		}
		if *g.ID == w.MemberLdapGroup {
			memberLdapGroupExist = true
		}
		if *g.ID == w.SpeakerLdapGroup {
			speakerLdapGroupExist = true
		}
	}
	w.Valid = allowGroupExist && excludeGroupExist && memberLdapGroupExist && speakerLdapGroupExist
}

func (w Workgroup) IsValid() bool {
	return w.Valid
}
