package main

import (
	"fmt"
	"os"
	"time"

	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"
	server "gitlab.com/Felix-Ulonska/wgmanager/http"
	config "gitlab.com/Felix-Ulonska/wgmanager/utils/config_editor"
	keycloak "gitlab.com/Felix-Ulonska/wgmanager/utils/keycloak"
	"gitlab.com/Felix-Ulonska/wgmanager/utils/mail"
	"gitlab.com/Felix-Ulonska/wgmanager/utils/mailman3"
)

func getEnv(key, fallback string, secret bool) string {
	if value, ok := os.LookupEnv(key); ok {
		if !secret {
			log.Debugf(`Setting: %s to %s`, key, fallback)
		}
		return value
	}
	if !secret {
		log.Debugf(`Fallback: set %s to %s`, key, fallback)
	}
	return fallback
}

func getNonNullableEnv(key string, secret bool) string {
	if value, ok := os.LookupEnv(key); ok {
		if !secret {
			log.Debugf(`Using: %s to %s`, key, value)
		}
		return value
	}
	if !secret {
		log.Fatal(fmt.Sprintf("%s needs to be set", key))
	}
	return ""
}

func main() {
	log.SetLevel(log.DebugLevel)
	log.Info(`Strating: 
 __      __    __  __                             
 \ \    / /_ _|  \/  |__ _ _ _  __ _ __ _ ___ _ _ 
  \ \/\/ / _  | |\/| / _\  | ' \/ _  / _  / -_) '_|
   \_/\_/\__, |_|  |_\__,_|_||_\__,_\__, \___|_|  
         |___/                      |___/         `)
	err := godotenv.Load()
	if err != nil {
		log.Warning("Unable to load godotenv", err)
	}

	log.Debug("Loading Environment varibles")

	configLocation := getEnv("CONFIG_PATH", "config.json", false)

	// Location of assets which should be served as frontend
	frontendLocation := getEnv("FRONTEND_LOCAITON", "frontend/", false)

	adminGroup := getNonNullableEnv("ADMIN_GROUP_PATH", false)

	keycloakPath := getEnv("KEYCLOAK_PATH", "http://localhost:8080", false)
	keycloakUser := getNonNullableEnv("KEYCLOAK_USER", false)
	keycloakPass := getNonNullableEnv("KEYCLOAK_PASS", true)
	keycloakRealm := getNonNullableEnv("KEYCLOAK_REALM", false)

	mailUser := getNonNullableEnv("SMTP_USER", false)
	mailSenderMail := getNonNullableEnv("SMTP_SENDER_MAIL", false)
	mailPassword := getNonNullableEnv("SMTP_PASS", true)
	mailHost := getNonNullableEnv("SMTP_HOST", false)

	restMailmanUser := getNonNullableEnv("MAILMAN_REST_USER", false)
	restMailmanPass := getNonNullableEnv("MAILMAN_REST_PASSWORD", true)
	restMailmanHost := getNonNullableEnv("MAILMAN_REST_HOST", false)

	enableDebugEndpoints := getEnv("TESTING_MODE", "false", false) == "true"
	port := getEnv("PORT", "8000", false)

	config.InitConfig(configLocation)

	mailtemplateLocation := getEnv("MAILTEMPLATES_LOCATION", "mailtemplates", false)

	mail.InitMail(mailUser, mailPassword, mailHost, mailSenderMail, mailtemplateLocation)

	keycloak.InitKeycloak(keycloakPath, keycloakUser, keycloakPass, keycloakRealm)

	mailman3.InitMailman(restMailmanHost, restMailmanUser, restMailmanPass)
	mailman3.StartBackgroundSync(time.Duration(1 * time.Second))

	gitCommitHash := getEnv("CI_COMMIT_SHA", "local build", false)

	server.StartHttpServer(frontendLocation, port, enableDebugEndpoints, adminGroup, gitCommitHash)
}
