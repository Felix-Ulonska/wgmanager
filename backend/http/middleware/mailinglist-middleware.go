package middleware

import (
	"net/http"

	"github.com/gin-gonic/gin"
	configeditor "gitlab.com/Felix-Ulonska/wgmanager/utils/config_editor"
)

func AddMailingToContext() gin.HandlerFunc {
	return func(c *gin.Context) {
		mlUuid, found := c.Params.Get("uuid")
		if !found {
			c.AbortWithStatus(400)
			return
		}
		mailinglists := configeditor.GetConfig().Mailinglists()
		found = false
		for _, ml := range mailinglists {
			if ml.UUID == mlUuid {
				c.Set("mailinglist", ml)
				found = true
			}
		}
		if !found {
			c.AbortWithStatus(http.StatusInternalServerError)
		}
	}
}
