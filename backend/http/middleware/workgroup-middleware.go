package middleware

import (
	"net/http"

	"github.com/gin-gonic/gin"
	configeditor "gitlab.com/Felix-Ulonska/wgmanager/utils/config_editor"
)

func AddWorkgroupInContext() gin.HandlerFunc {
	return func(c *gin.Context) {
		wgUuid, found := c.Params.Get("uuid")
		if !found {
			c.AbortWithStatus(400)
			return
		}
		wgs := configeditor.GetConfig().Workgroups()
		found = false
		for _, w := range wgs {
			if w.UUID == wgUuid {
				c.Set("workgroup", w)
				found = true
			}
		}
		if !found {
			c.AbortWithStatus(http.StatusInternalServerError)
		}
	}
}
