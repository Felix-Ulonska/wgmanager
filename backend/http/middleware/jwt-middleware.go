package middleware

import (
	"errors"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

type JWTValidator interface {
	IsUserInJWTGroup(jwt string, groupName string) (bool, error)
	IsValidJWT(jwt string) (bool, error)
	ValidateAndGetUserId(jwt string) (string, error)
	JwtHasVerifiedEmail(jwt string) (bool, error)
}

// TODO following should be two methods

/// Also checks if email is verified
func IsValidJWTMiddleware(jwtValidator JWTValidator) gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := strings.Replace(c.GetHeader("Authorization"), "Bearer ", "", 1)
		if authHeader == "" {
			log.Debugln("Rejected token, Missing authHeader")
			c.AbortWithStatus(http.StatusUnauthorized)
		}

		verifiedEmail, err := jwtValidator.JwtHasVerifiedEmail(authHeader)
		if err != nil {
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		} else if !verifiedEmail {
			c.Error(errors.New("Not verified Email"))
			c.AbortWithStatus(http.StatusUnauthorized)
		}
		userId, err := jwtValidator.ValidateAndGetUserId(authHeader)
		// TODO test if err is thrown when JWT is not valid.
		if err != nil {
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}
		c.Set("userId", userId)
		c.Next()
	}
}

// Rejects request if user is not authenticated or is not in the valid group
func AuthenticationMiddleware(neededGroup string, jwtValidator JWTValidator) gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := strings.Replace(c.GetHeader("Authorization"), "Bearer ", "", 1)
		if authHeader == "" {
			log.Debugln("Rejected token, Missing authHeader")
			c.AbortWithStatus(http.StatusUnauthorized)
		}

		inGroup, err := jwtValidator.IsUserInJWTGroup(authHeader, neededGroup)
		if err != nil {
			log.Debugln("Rejected token becuase of validation error", err)
			c.AbortWithStatus(http.StatusInternalServerError)
		} else if !inGroup {
			log.Debugf("Rejected token becuase not in Group %s\n", neededGroup)
			c.AbortWithStatus(http.StatusUnauthorized)
		} else {
			c.Next()
		}
	}
}
