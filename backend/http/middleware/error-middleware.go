package middleware

import "github.com/gin-gonic/gin"

type HttpError struct {
	Error string `json:"error"`
}

func ErrorMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()

		errors := c.Errors.ByType(gin.ErrorTypePublic)
		errors = append(errors, c.Errors.ByType(gin.ErrorTypeAny)...)
		if len(errors) > 0 {
			c.JSON(-1, HttpError{c.Errors.Last().Error()})
		}
	}
}
