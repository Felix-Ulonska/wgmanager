package rest

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func AddHealthEndpointsToGroup(group *gin.RouterGroup) {
	group.GET("/testAuthentication", testAuthenticationHandler)
}

// Handler to check working authentication
// @summary check working authentication
// @Success 200 {string} Token "works"
// @Failure 400
// @Router /api/v1/health/testAuthentication [get]
func testAuthenticationHandler(c *gin.Context) {
	c.String(http.StatusOK, "works")
}
