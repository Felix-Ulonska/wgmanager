package testing

import (
	"net/http"

	"github.com/gin-gonic/gin"
	configeditor "gitlab.com/Felix-Ulonska/wgmanager/utils/config_editor"
)

// Resets the backend for e2ee test
// @Success 200
// @Router /api/v1/testing/reset [post]
func reset(ctx *gin.Context) {
	config := configeditor.GetConfig()
	config.Reset()
	ctx.Status(http.StatusOK)
}
