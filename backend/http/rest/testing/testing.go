// In this package are Endpoints which can reset the backend. These Endpoints should only be active while testing.
package testing

import "github.com/gin-gonic/gin"

// NEVER call this method in a prod deployment
func AddTestingEndpoints(group *gin.RouterGroup) *gin.RouterGroup {
	testingGroup := group.Group("/testing")
	testingGroup.POST("reset", reset)
	return testingGroup
}
