package rest

import (
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/Felix-Ulonska/wgmanager/utils/keycloak"
)

func GetUserId(ctx *gin.Context) (string, error) {
	jwt := strings.Replace(ctx.GetHeader("Authorization"), "Bearer ", "", 1)
	k := keycloak.GetKeycloak()
	return k.ValidateAndGetUserId(jwt)
}
