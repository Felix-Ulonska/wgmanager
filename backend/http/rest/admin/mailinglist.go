package admin

import (
	"net/http"

	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	configeditor "gitlab.com/Felix-Ulonska/wgmanager/utils/config_editor"
	"gitlab.com/Felix-Ulonska/wgmanager/utils/mailman3"

	"github.com/gin-gonic/gin"
	"gitlab.com/Felix-Ulonska/wgmanager/models"
)

// Returns all Mailinglists
// @Success 200 {object} []models.Mailinglist
// @Accept  json
// @Produce  json
// @Router /api/v1/admin/mailinglists [get]
func getMailingslists(ctx *gin.Context) {
	configEditor := configeditor.GetConfig()
	mailinglists := configEditor.Mailinglists()
	ctx.JSONP(http.StatusOK, mailinglists)
}

// Edits a mailinglist
// @Param mailinglist body models.Mailinglist true "New Mailinglist"
// @Accept  json
// @Produce  json
// @Success 200
// @Failure 403 Mailinglist does not exist
// @Router /api/v1/admin/mailinglist [put]
func editMailinglist(ctx *gin.Context) {
	var newMl models.Mailinglist
	err := ctx.BindJSON(&newMl)
	if err != nil {
		ctx.Status(http.StatusBadRequest)
		return
	}

	config := configeditor.GetConfig()
	newMl.Valid = true
	err = config.EditMailinglist(newMl)
	var notFoundError *configeditor.MailinglistNotFoundError
	if errors.As(err, &notFoundError) {
		ctx.Status(http.StatusNotFound)
		return
	} else if err != nil {
		ctx.Status(http.StatusInternalServerError)
		return
	} else {
		ctx.Status(http.StatusOK)
	}
}

type addMailinglistDto struct {
	Name                   string `json:"name" binding:"required"`
	Email                  string `json:"email" binding:"required"`
	BaseGroup              string `json:"baseGroup" binding:"required"`     // Members of this group will be added to mailinglist if ModifierGroupIsInclude=true else these users are added to group if inside ModifierGroup
	ModifierGroup          string `json:"modifierGroup" binding:"required"` // Members of this group will be added if modifierGroupIsInclude else these users will be removed
	ModifierGroupIsInclude bool   `json:"modifierGroupIsInclude" binding:"required"`
}

// Adds a mailinglist
// @Param mailinglist body addMailinglistDto true "New Mailinglist"
// @Accept  json
// @Produce  json
// @Success 201 {object} models.Mailinglist
// @Router /api/v1/admin/mailinglist [post]
func addMailinglist(ctx *gin.Context) {
	var newMl addMailinglistDto
	// TODO check that the request is really aborted
	err := ctx.BindJSON(&newMl)
	if err != nil {
		ctx.Status(http.StatusBadRequest)
	}
	configEditor := configeditor.GetConfig()
	createdMl := configEditor.AddMailinglist(
		models.Mailinglist{
			UUID:                   uuid.NewString(),
			Name:                   newMl.Name,
			BaseGroup:              newMl.BaseGroup,
			ModifierGroup:          newMl.ModifierGroup,
			ModifierGroupIsInclude: newMl.ModifierGroupIsInclude,
			Email:                  newMl.Email,
			Valid:                  true,
		})
	ctx.JSON(http.StatusCreated, createdMl)
}

// Delete a mailinglist
// @Param uuid path string true "UUID of the Mailinglist which should be deleeteted"
// @Accept  json
// @Produce  json
// @Success 200
// @Router /api/v1/admin/mailinglist/{uuid} [delete]
func deleteMailinglist(ctx *gin.Context) {
	uuid, found := ctx.Params.Get("uuid")
	if !found {
		ctx.Status(http.StatusBadRequest)
		return
	}

	config := configeditor.GetConfig()
	err := config.DeleteMailinglist(uuid)
	var notFoundError *configeditor.MailinglistNotFoundError
	if errors.As(err, &notFoundError) {
		ctx.Status(http.StatusNotFound)
		return
	} else if err != nil {
		ctx.Status(http.StatusInternalServerError)
		return
	} else {
		ctx.Status(http.StatusOK)
	}
}

type AvailableMaillistsDto struct {
	MailAddress string `json:"mailAddress"`
	MemberCount int    `json:"memberCount"`
	Used        bool   `json:"used"`
}

// Returns all Mailaddresses which can be used as Mail Adress in Group
// @Success 200 {object} []AvailableMaillistsDto
// @Accept  json
// @Produce  json
// @Router /api/v1/admin/availableMaillistFromMailman [get]
func getAvailableMaillistFromMailman(ctx *gin.Context) {
	lists, err := mailman3.GetMailmanRestClient().GetAllLists()
	if err != nil {
		logrus.Warn("Failed getAvailableMaillistFromMailman" + err.Error())
		ctx.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	usedMails := configeditor.GetConfig().GetAllUsedMails()

	responseLists := make([]AvailableMaillistsDto, len(lists))
	for i, md := range lists {

		used := false
		for _, v := range usedMails {
			if v == md.FqdnListname {
				used = true
			}
		}
		responseLists[i] = AvailableMaillistsDto{MailAddress: md.FqdnListname, MemberCount: md.MemberCount, Used: used}
	}
	ctx.JSON(http.StatusOK, responseLists)
}
