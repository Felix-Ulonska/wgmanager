package admin

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Felix-Ulonska/wgmanager/http/middleware"
	"gitlab.com/Felix-Ulonska/wgmanager/utils/keycloak"
)

func AddAdminToEndpoints(group *gin.RouterGroup, adminKeycloakGroup string) *gin.RouterGroup {
	adminGroup := group.Group("admin")
	adminGroup.Use(middleware.AuthenticationMiddleware(adminKeycloakGroup, keycloak.GetKeycloak()))
	adminGroup.GET("/isAdmin", isAdmin)
	adminGroup.GET("/mailinglists", getMailingslists)
	adminGroup.POST("/mailinglist", addMailinglist)
	adminGroup.PUT("/mailinglist", editMailinglist)
	adminGroup.GET("/allAvailableGroups", allAvailableGroups)
	adminGroup.DELETE("/mailinglist/:uuid", deleteMailinglist)

	adminGroup.GET("/availableMaillistFromMailman", getAvailableMaillistFromMailman)

	adminGroup.POST("/workgroup", addWorkgroup)
	adminGroup.GET("/workgroups", getWorkgroups)
	adminGroup.PUT("/workgroup", editWorkgroup)
	adminGroup.POST("/workgroup/:wgId/speaker/:userId", addSpeaker)
	adminGroup.DELETE("/workgroup/:wgId/speaker/:userId", removeSpeaker)
	adminGroup.DELETE("/workgroup/:wgId", deleteWorkgroup)
	return adminGroup
}

// Test Endpoint to check if Admin
// @Success 200
// @Failure 400
// @Router /api/v1/admin/isAdmin [get]
func isAdmin(ctx *gin.Context) {
	ctx.Status(200)
}
