package admin

import (
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gitlab.com/Felix-Ulonska/wgmanager/models"
	"gitlab.com/Felix-Ulonska/wgmanager/utils/keycloak"
	"net/http"
)

// Get All Avaible Groups
// @Produce json
// @Success 200 {array} models.LdapGroup
// @Failure 500
// @Router /api/v1/admin/allAvailableGroups [get]
func allAvailableGroups(ctx *gin.Context) {
	groups, err := keycloak.GetKeycloak().AvaibleGruops()
	if err != nil {
		log.Error("Failed getting Avaible Groups from keycloak", err)
		ctx.Status(http.StatusInternalServerError)
	} else {
		retGroups := make([]models.LdapGroup, len(groups))
		for i, k := range groups {
			retGroups[i] = models.LdapGroup{Name: *k.Name, Id: *k.ID, Path: *k.Path}
		}
		ctx.JSONP(http.StatusOK, retGroups)
	}
}
