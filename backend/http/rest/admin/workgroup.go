package admin

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/Felix-Ulonska/wgmanager/models"
	configeditor "gitlab.com/Felix-Ulonska/wgmanager/utils/config_editor"
)

// Edits a workgroup
// @Param workgroup body models.Workgroup true "Edited Workgroup"
// @Accept  json
// @Produce  json
// @Success 200
// @Failure 403 Workroup does not exist
// @Router /api/v1/admin/workgroup [put]
func editWorkgroup(ctx *gin.Context) {
	var newWg models.Workgroup
	err := ctx.BindJSON(&newWg)
	if err != nil {
		ctx.Status(http.StatusBadRequest)
		return
	}

	config := configeditor.GetConfig()
	newWg.Valid = true
	err = config.EditWorkgroup(newWg)
	var notFoundError *configeditor.WorkgroupNotFoundError
	if errors.As(err, &notFoundError) {
		ctx.Status(http.StatusNotFound)
		return
	} else if err != nil {
		ctx.Status(http.StatusInternalServerError)
		return
	} else {
		ctx.Status(http.StatusOK)
	}
}

type WorkgroupAddDto struct {
	AllowJoinLdapGroup          string `json:"allowJoinLdapGroup" binding:"required,uuid"`
	ExcludeMailinglistLdapGroup string `json:"excludeMailinglistLdapGroup" binding:"required,uuid"`
	Mail                        string `json:"mail" binding:"omitempty,email"`
	MatterMostLink              string `json:"matterMostLink" binding:"omitempty,url"`
	MemberLdapGroup             string `json:"memberLdapGroup" binding:"required,uuid"`
	Name                        string `json:"name" binding:"required,min=2,max=100" `
	SpeakerLdapGroup            string `json:"speakerLdapGroup" binding:"required,uuid"`
}

// Add a workgroup
// @Success 200 {object} WorkgroupAddDto
// @Param workgroup body WorkgroupAddDto true "New Workgroup"
// @Accept  json
// @Produce  json
// @Router /api/v1/admin/workgroup [post]
func addWorkgroup(ctx *gin.Context) {
	var newWorkgroup WorkgroupAddDto
	err := ctx.BindJSON(&newWorkgroup)
	if err != nil {
		ctx.Error(err)
		ctx.Status(http.StatusBadRequest)
		return
	}
	configEditor := configeditor.GetConfig()
	createdWorkgroup, err := configEditor.AddWorkgoup(
		models.Workgroup{
			UUID:                        uuid.NewString(),
			Name:                        newWorkgroup.Name,
			AllowJoinLdapGroup:          newWorkgroup.AllowJoinLdapGroup,
			MemberLdapGroup:             newWorkgroup.MemberLdapGroup,
			Mail:                        newWorkgroup.Mail,
			MatterMostLink:              newWorkgroup.MatterMostLink,
			ExcludeMailinglistLdapGroup: newWorkgroup.ExcludeMailinglistLdapGroup,
			SpeakerLdapGroup:            newWorkgroup.SpeakerLdapGroup,
			Valid:                       true,
		})
	if err != nil {
		ctx.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	ctx.JSON(http.StatusCreated, createdWorkgroup)
}

// Returns all Workgroups
// @Success 200 {object} []models.Workgroup
// @Accept  json
// @Produce  json
// @Router /api/v1/admin/workgroups [get]
func getWorkgroups(ctx *gin.Context) {
	configEditor := configeditor.GetConfig()
	workgroups := configEditor.Workgroups()
	ctx.JSONP(http.StatusOK, workgroups)
}

// Delete a workgroup
// @Param wgId path string true "UUID of the Workgroup which should be deleeteted"
// @Accept  json
// @Produce  json
// @Success 200
// @Router /api/v1/admin/workgroup/{wgId} [delete]
func deleteWorkgroup(ctx *gin.Context) {
	uuid, found := ctx.Params.Get("wgId")
	if !found {
		ctx.Error(errors.New("Missing wgId"))
		ctx.Status(http.StatusBadRequest)
		return
	}

	config := configeditor.GetConfig()
	err := config.DeleteWorkgroup(uuid)
	var notFoundError *configeditor.WorkgroupNotFoundError
	if errors.As(err, &notFoundError) {
		ctx.Status(http.StatusNotFound)
		return
	} else if err != nil {
		ctx.Status(http.StatusInternalServerError)
		return
	} else {
		ctx.Status(http.StatusOK)
	}
}

// add Speaker to workgroup
// @Param wgId path string true "UUID of workgroup to which the user should be added"
// @Param userId path string true "userId of User which should be speaker"
// @Router /api/v1/admin/workgroup/{wgId}/speaker/{userId} [post]
func addSpeaker(ctx *gin.Context) {
	wgId, found := ctx.Params.Get("wgId")
	if !found {
		ctx.Status(http.StatusBadRequest)
		return
	}
	config := configeditor.GetConfig()
	wg, err := config.Workgroup(wgId)
	if err != nil {
		ctx.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	userId, found := ctx.Params.Get("userId")
	if !found {
		ctx.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	err = wg.AddSpeaker(userId)
	if err != nil {
		ctx.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	ctx.Status(http.StatusOK)
}

// remove Speaker from workgroup
// @Param wgId path string true "UUID of workgroup to which the user should be added"
// @Param userId path string true "userId of User which should be removed"
// @Router /api/v1/admin/workgroup/{wgId}/speaker/{userId} [delete]
func removeSpeaker(ctx *gin.Context) {
	wgId, found := ctx.Params.Get("wgId")
	if !found {
		ctx.Status(http.StatusBadRequest)
		return
	}
	config := configeditor.GetConfig()
	wg, err := config.Workgroup(wgId)
	if err != nil {
		ctx.Status(http.StatusInternalServerError)
		return
	}

	userId, found := ctx.Params.Get("userId")
	if !found {
		ctx.Status(http.StatusBadRequest)
		return
	}

	err = wg.RemoveSpeaker(userId)
	if err != nil {
		ctx.Status(http.StatusInternalServerError)
		return
	}

	ctx.Status(http.StatusOK)
}
