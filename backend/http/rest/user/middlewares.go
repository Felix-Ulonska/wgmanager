package user

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/Felix-Ulonska/wgmanager/http/middleware"
	"gitlab.com/Felix-Ulonska/wgmanager/models"
)

func IsAllowedToJoinToWorkgroup() gin.HandlerFunc {
	return func(c *gin.Context) {
		userId := c.GetString("userId")
		if userId == "" {
			c.Abort()
			return
		}
	}
}

func isAllowedToJoinWorkgroup() gin.HandlerFunc {
	return func(c *gin.Context) {
		userId, userIdSet := c.Get("userId")
		if !userIdSet {
			c.AbortWithStatus(http.StatusBadRequest)
		}
		workgroup, workgroupSet := c.Get("workgroup")
		if !workgroupSet {
			c.AbortWithStatus(http.StatusBadRequest)
		}
		wg, ok := workgroup.(models.Workgroup)
		if ok == false {
			c.AbortWithStatus(http.StatusBadRequest)
		}
		allowed, err := wg.IsUserAllowedToJoin(userId.(string))
		if err != nil {
			c.AbortWithError(401, err)
		}
		if !allowed {
			c.AbortWithStatusJSON(http.StatusUnauthorized, middleware.HttpError{Error: errors.New("Not Allowed to join Group").Error()})
		}
		c.Next()
	}
}

func isAllowedToJoinMailinglist() gin.HandlerFunc {
	return func(c *gin.Context) {
		userId, userIdSet := c.Get("userId")
		if !userIdSet {
			c.AbortWithStatus(http.StatusBadRequest)
		}
		mailinglist, mailinglistSet := c.Get("mailinglist")
		if !mailinglistSet {
			c.AbortWithStatus(http.StatusBadRequest)
		}
		ml, ok := mailinglist.(models.Mailinglist)
		if ok == false {
			c.AbortWithStatus(http.StatusBadRequest)
		}
		allowed, err := ml.IsUserAllowedToJoin(userId.(string))
		if err != nil {
			logrus.Warn("Not allowed to join", err)
			c.AbortWithError(401, err)
		}
		if !allowed {
			c.AbortWithStatusJSON(http.StatusUnauthorized, middleware.HttpError{Error: errors.New("Not Allowed to join Mailinglist").Error()})
		}
		c.Next()
	}
}
