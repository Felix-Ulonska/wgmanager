package user

import (
	"net/http"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gitlab.com/Felix-Ulonska/wgmanager/http/rest"
	"gitlab.com/Felix-Ulonska/wgmanager/models"
	configeditor "gitlab.com/Felix-Ulonska/wgmanager/utils/config_editor"
)

type WorkgroupUserDto struct {
	UUID                    string `json:"uuid" binding:"required"`
	Name                    string `json:"name" binding:"required"`
	Email                   string `json:"email" binding:"required"`
	AllowedToJoin           bool   `json:"allowedToJoin" binding:"required"`
	Joined                  bool   `json:"joined" binding:"required"`
	SubscribedToMailinglist bool   `json:"subscribedToMailinglist" binding:"required"`
	IsSpeaker               bool   `json:"isSpeaker" binding:"required"`
	MattermostLink          string `json:"mattermostLink"`
	Valid                   bool   `json:"valid"`
}

// Returns all Workgroups
// @Success 200 {object} []WorkgroupUserDto
// @Produce  json
// @Router /api/v1/user/workgroups [get]
func getWorkgroups(ctx *gin.Context) {
	configEditor := configeditor.GetConfig()
	workgroups := configEditor.Workgroups()

	userId, err := rest.GetUserId(ctx)
	if err != nil {
		log.Warn("getMailingslists error", err)
		ctx.Status(http.StatusInternalServerError)
		return
	}

	workgroupsForReturn := make([]WorkgroupUserDto, 0)
	for _, w := range workgroups {
		// Ignore errors if workgroup is not valid. The client should not show a workgroup which is not valid
		joined, err := w.IsUserJoined(userId)
		if w.Valid && err != nil {
			log.Warn("getMailingslists error", err)
			ctx.Status(http.StatusInternalServerError)
			return
		}
		isSpeaker, err := w.IsSpeaker(userId)
		if w.Valid && err != nil {
			ctx.Status(http.StatusInternalServerError)
			return
		}
		canJoin, err := w.IsUserAllowedToJoin(userId)
		if w.Valid && err != nil {
			ctx.Status(http.StatusInternalServerError)
			return
		}
		subscribedToMailinglist, err := w.IsUserSubscribedToMailinglist(userId)
		if w.Valid && err != nil {
			ctx.Status(http.StatusInternalServerError)
			return
		}
		workgroupsForReturn = append(workgroupsForReturn, WorkgroupUserDto{
			UUID:                    w.UUID,
			Name:                    w.Name,
			Email:                   w.Mail,
			Joined:                  joined,
			AllowedToJoin:           canJoin,
			SubscribedToMailinglist: subscribedToMailinglist,
			MattermostLink:          w.MatterMostLink,
			IsSpeaker:               isSpeaker,
			Valid:                   w.Valid,
		})
	}

	ctx.JSONP(http.StatusOK, workgroupsForReturn)
}

// Join Workgroup
// @Success 200
// @Error 400 HttpError
// @Param uuid path string true "Workgroup which should be joined"
// @Router /api/v1/user/workgroup/{uuid}/join [post]
func joinWorkgroup(ctx *gin.Context) {
	workgroup, found := ctx.Get("workgroup")
	if !found {
		ctx.Status(500)
	}
	err := workgroup.(models.Workgroup).AddUser(ctx.GetString("userId"), false)
	if err != nil {
		ctx.Error(err)
	}
	ctx.Status(http.StatusOK)
}

// Leave Workgroup
// @Success 200
// @Error 400 HttpError
// @Param uuid path string true "Workgroup which should be joined"
// @Router /api/v1/user/workgroup/{uuid}/leave [post]
func leaveWorkgroup(ctx *gin.Context) {
	workgroup, found := ctx.Get("workgroup")
	if !found {
		ctx.Status(500)
	}
	err := workgroup.(models.Workgroup).DeleteUser(ctx.GetString("userId"))
	if err != nil {
		ctx.Error(err)
	}
	ctx.Status(http.StatusOK)
}

// Join Mailinglist of Workgroup
// @Success 200
// @Param uuid path string true "Workgroup which should be joined"
// @Router /api/v1/user/workgroup/{uuid}/joinMailinglist [post]
func joinMailinglistOfWorkgroup(ctx *gin.Context) {
	workgroup, found := ctx.Get("workgroup")
	if !found {
		ctx.Status(500)
	}
	err := workgroup.(models.Workgroup).AddUserToMailinglist(ctx.GetString("userId"))
	if err != nil {
		ctx.Error(err)
	}
	ctx.Status(http.StatusOK)
}

// Leave Mailinglist of Workgroup
// @Success 200
// @Param uuid path string true "Workgroup which should be joined"
// @Router /api/v1/user/workgroup/{uuid}/leaveMailinglist [post]
func leaveMailinglistOfWorkgroup(ctx *gin.Context) {
	workgroup, found := ctx.Get("workgroup")
	if !found {
		ctx.Status(500)
	}
	err := workgroup.(models.Workgroup).RemoveUserFromMailinglist(ctx.GetString("userId"))
	if err != nil {
		ctx.Error(err)
	}
	ctx.Status(http.StatusOK)
}

type WgMemberDto struct {
	Id        string `json:"id" binding:"required"`
	Mail      string `json:"mail" binding:"required"`
	FirstName string `json:"firstName" binding:"required"`
	LastName  string `json:"lastName" binding:"required"`
	Speaker   bool   `json:"speaker" binding:"required"`
}

// Members of Workgroup
// @Success 200 {object} []WgMemberDto
// @Param uuid path string true "Workgroup which should be joined"
// @Router /api/v1/user/workgroup/{uuid}/members [get]
func getWorkgroupMembers(ctx *gin.Context) {
	workgroup, found := ctx.Get("workgroup")
	if !found {
		ctx.Status(500)
	}
	members, err := workgroup.(models.Workgroup).Members()
	if err != nil {
		ctx.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	membersReturn := make([]WgMemberDto, len(members))
	for i, m := range members {

		membersReturn[i] = WgMemberDto{Id: *m.ID}
		if m.Email != nil {
			membersReturn[i].Mail = *m.Email
		}
		if m.FirstName != nil {
			membersReturn[i].FirstName = *m.FirstName
		}
		if m.LastName != nil {
			membersReturn[i].LastName = *m.LastName
		}
		isSpeaker, err := workgroup.(models.Workgroup).IsSpeaker(*m.ID)
		if err != nil {
			ctx.AbortWithError(http.StatusInternalServerError, err)
			return
		}
		membersReturn[i].Speaker = isSpeaker
	}
	ctx.JSON(http.StatusOK, membersReturn)
}
