package user

import (
	"net/http"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gitlab.com/Felix-Ulonska/wgmanager/http/rest"
	"gitlab.com/Felix-Ulonska/wgmanager/models"
	configeditor "gitlab.com/Felix-Ulonska/wgmanager/utils/config_editor"
)

type MailinglistUserDto struct {
	UUID            string `json:"uuid"`
	Name            string `json:"name" binding:"required"`
	Email           string `json:"email" binding:"required"`
	AllowedToJoin   bool   `json:"allowedToJoin"`
	Joined          bool   `json:"joined"`
	NumberOfMembers int    `json:"numberOfMembers" binding:"required"`
	Valid           bool   `json:"valid" binding:"required"`
}

// Returns all Mailinglists
// @Success 200 {object} []MailinglistUserDto
// @Produce  json
// @Router /api/v1/user/mailinglists [get]
func getMailingslists(ctx *gin.Context) {
	configEditor := configeditor.GetConfig()
	mailinglists := configEditor.Mailinglists()

	userId, err := rest.GetUserId(ctx)
	if err != nil {
		log.Warn("getMailingslists error", err)
		ctx.Status(http.StatusInternalServerError)
		return
	}

	mailinglistsForReturn := make([]MailinglistUserDto, len(mailinglists))
	for i, m := range mailinglists {
		// Errors are going to be ignored if the mailinglist is not valid
		joined, err := m.IsUserInMailinglist(userId)
		if m.Valid && err != nil {
			log.Warn("getMailingslists error", err)
			ctx.Status(http.StatusInternalServerError)
			return
		}

		members, err := m.Members()
		if m.Valid && err != nil {
			ctx.Status(http.StatusInternalServerError)
			return
		}
		canJoin, err := m.IsUserAllowedToJoin(userId)
		if m.Valid && err != nil {
			ctx.Status(http.StatusInternalServerError)
			return
		}
		mailinglistsForReturn[i] = MailinglistUserDto{
			UUID:            m.UUID,
			Name:            m.Name,
			Email:           m.Email,
			AllowedToJoin:   canJoin,
			Joined:          joined,
			NumberOfMembers: len(members),
			Valid:           m.Valid,
		}
	}

	ctx.JSONP(http.StatusOK, mailinglistsForReturn)
}

// Join Mailinglist
// @Success 200
// @Param uuid path string true "Mailinglist which should be joined"
// @Router /api/v1/user/mailinglist/{uuid}/join [post]
func joinMailinglist(ctx *gin.Context) {
	ml, found := ctx.Get("mailinglist")
	if !found {
		ctx.Status(http.StatusInternalServerError)
	}
	err := ml.(models.Mailinglist).AddUser(ctx.GetString("userId"))
	if err != nil {
		ctx.Error(err)
	}
	ctx.Status(http.StatusOK)
}

// leave Mailinglist
// @Param uuid path string true "UUID of mailnglist which should be left"
// @Router /api/v1/user/mailinglist/{uuid}/leave [post]
func leaveMailinglist(ctx *gin.Context) {
	ml, found := ctx.Get("mailinglist")
	if !found {
		ctx.Status(http.StatusInternalServerError)
	}
	err := ml.(models.Mailinglist).RemoveUser(ctx.GetString("userId"))
	if err != nil {
		ctx.Error(err)
	}
	ctx.Status(http.StatusOK)
}
