package user

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Felix-Ulonska/wgmanager/http/middleware"
	"gitlab.com/Felix-Ulonska/wgmanager/utils/keycloak"
)

func AddUserEndpoints(group *gin.RouterGroup) {
	userGroup := group.Group("user")

	userGroup.Use(middleware.IsValidJWTMiddleware(keycloak.GetKeycloak()))
	userMailling := userGroup.Group("", middleware.AddMailingToContext(), isAllowedToJoinMailinglist())

	userGroup.GET("/mailinglists", getMailingslists)
	userMailling.POST("/mailinglist/:uuid/join", isAllowedToJoinMailinglist(), middleware.AddMailingToContext(), joinMailinglist)
	userMailling.POST("/mailinglist/:uuid/leave", middleware.AddMailingToContext(), leaveMailinglist)

	userWorkgroup := userGroup.Group("", middleware.AddWorkgroupInContext())
	userGroup.GET("/workgroups", getWorkgroups)
	userWorkgroup.POST("/workgroup/:uuid/join", IsAllowedToJoinToWorkgroup(), joinWorkgroup)
	userWorkgroup.POST("/workgroup/:uuid/leave", leaveWorkgroup)
	userWorkgroup.GET("/workgroup/:uuid/members", getWorkgroupMembers)

	userWorkgroup.POST("/workgroup/:uuid/joinMailinglist", joinMailinglistOfWorkgroup)
	userWorkgroup.POST("/workgroup/:uuid/leaveMailinglist", leaveMailinglistOfWorkgroup)
}
