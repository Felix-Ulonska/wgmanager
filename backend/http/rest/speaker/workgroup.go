package speaker

import (
	"errors"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/Felix-Ulonska/wgmanager/models"
	"gitlab.com/Felix-Ulonska/wgmanager/utils/keycloak"
)

// AddUser to workgroup
// @Param uuid path string true "UUID of workgroup to which the user should be added"
// @Param userMail path string true "Mail of user which should be added"
// @Router /api/v1/speaker/workgroup/{uuid}/members/{userMail} [post]
func AddUser(ctx *gin.Context) {
	workgroup, found := ctx.Get("workgroup")
	if !found {
		ctx.Error(errors.New("Workgroup parameter missing"))
		ctx.Status(http.StatusInternalServerError)
		return
	}
	userMail, found := ctx.Params.Get("userMail")
	userMail = strings.TrimSpace(userMail)
	if !found {
		ctx.Error(errors.New("Usermail parameter missing"))
		ctx.Status(http.StatusBadRequest)
		return
	}

	user, err := keycloak.GetKeycloak().GetUserByEmail(userMail)
	if err != nil && err.Error() == keycloak.ERR_USER_NOT_FOUND {
		ctx.Error(errors.New("User not found"))
		ctx.Status(http.StatusNotFound)
		return
	} else if err != nil {
		ctx.Status(http.StatusInternalServerError)
		return
	}

	err = workgroup.(models.Workgroup).AddUser(*user.ID, true)
	if err != nil {
		ctx.Status(http.StatusInternalServerError)
		return
	}

	ctx.Status(http.StatusOK)
}

// Remove User from Workgroup
// @Param uuid path string true "UUID of workgroup to which the user should be removed"
// @Param userId path string true "UUID of user which should be removed"
// @Router /api/v1/speaker/workgroup/{uuid}/members/{userId} [delete]
func RemoveUser(ctx *gin.Context) {
	workgroup, found := ctx.Get("workgroup")
	if !found {
		ctx.Status(http.StatusBadRequest)
		return
	}
	userId, found := ctx.Params.Get("userId")
	if !found {
		ctx.Status(http.StatusBadRequest)
		return
	}
	err := workgroup.(models.Workgroup).DeleteUser(userId)
	if err != nil {
		ctx.Status(http.StatusInternalServerError)
		return
	}

	ctx.Status(http.StatusOK)
}
