package speaker

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/Felix-Ulonska/wgmanager/http/middleware"
	"gitlab.com/Felix-Ulonska/wgmanager/models"
	"gitlab.com/Felix-Ulonska/wgmanager/utils/keycloak"
)

func speakerIsSpeakerOfGroup(ctx *gin.Context) {
	workgroup, found := ctx.Get("workgroup")
	if !found {
		ctx.Error(errors.New("Workgroup parameter missing"))
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}
	isSpeaker, err := workgroup.(models.Workgroup).IsSpeaker(ctx.GetString("userId"))
	if err != nil {
		ctx.AbortWithStatus(http.StatusInternalServerError)
		return
	} else if !isSpeaker {
		ctx.Error(errors.New("User not speaker"))
		ctx.AbortWithStatus(http.StatusUnauthorized)
		return
	}
	ctx.Next()
}

func AddSpeakerEndpoints(group *gin.RouterGroup) {
	speakerGroup := group.Group("speaker")

	// TODO check priviliges
	speakerGroup.Use(middleware.IsValidJWTMiddleware(keycloak.GetKeycloak()))

	speakerGroup.POST("/workgroup/:uuid/members/:userMail", middleware.AddWorkgroupInContext(), speakerIsSpeakerOfGroup, AddUser)
	speakerGroup.DELETE("/workgroup/:uuid/members/:userId", middleware.AddWorkgroupInContext(), speakerIsSpeakerOfGroup, speakerIsSpeakerOfGroup, RemoveUser)
}
