package server

import (
	"github.com/gin-gonic/gin"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/Felix-Ulonska/wgmanager/docs"
	"gitlab.com/Felix-Ulonska/wgmanager/http/middleware"
	"gitlab.com/Felix-Ulonska/wgmanager/http/rest"
	"gitlab.com/Felix-Ulonska/wgmanager/http/rest/admin"
	"gitlab.com/Felix-Ulonska/wgmanager/http/rest/speaker"
	"gitlab.com/Felix-Ulonska/wgmanager/http/rest/testing"
	"gitlab.com/Felix-Ulonska/wgmanager/http/rest/user"
)

// @title wgManager API
// @version 1.0

// @host localhost:8080
// @BasePath /api/v1
func StartHttpServer(frontendLocation string, port string, enableDebugEndpoints bool, adminGroup string, gitCommitVersion string) {
	r := gin.Default()

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	r.Use(middleware.ErrorMiddleware())

	apiGroup := r.Group("/api/v1")
	healthGroup := apiGroup.Group("health")
	rest.AddHealthEndpointsToGroup(healthGroup)
	admin.AddAdminToEndpoints(apiGroup, adminGroup)

	user.AddUserEndpoints(apiGroup)

	userGroup := apiGroup.Group("user")
	user.AddUserEndpoints(userGroup)

	speaker.AddSpeakerEndpoints(apiGroup)

	if enableDebugEndpoints {
		testing.AddTestingEndpoints(apiGroup)
	}

	apiGroup.GET("/version", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"gitCommitHash": gitCommitVersion,
		})
	})

	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	r.Run("0.0.0.0:" + port) // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
