package keycloak

import (
	"context"
	"fmt"
	"time"

	"github.com/Nerzal/gocloak/v8"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"

	"github.com/MicahParks/keyfunc"
)

type KeycloakConnection struct {
	token    *gocloak.JWT
	client   gocloak.GoCloak
	ctx      context.Context
	realm    string
	url      string
	user     string
	password string
	jwks     *keyfunc.JWKs
}

var (
	keycloakConnection KeycloakConnection
)

func GetKeycloak() *KeycloakConnection {
	return &keycloakConnection
}

func (k *KeycloakConnection) refreshTokenIfNeeded() error {
	// TODO the exp does not seem to work.
	// This does not validate. It just checks if the token is still is not yet expired
	/*
		token, _, err := new(jwt.Parser).ParseUnverified(k.token.AccessToken, jwt.MapClaims{})
		if err != nil {
			return err
		}
		if claims, ok := token.Claims.(jwt.MapClaims); ok {
			if int64(math.Round(claims["exp"].(float64))) < time.Now().Unix()-100 {
				k.refreshToken()
			}
		}
	*/
	// TODO patch back the logic
	return k.refreshToken()
}

func (k *KeycloakConnection) refreshToken() error {
	token, err := k.client.LoginAdmin(k.ctx, k.user, k.password, k.realm)
	if err != nil {
		log.Error("Cannot refresh token")
		return err
	}
	k.token = token
	return nil
}

func InitKeycloak(url string, user string, password string, realm string) {
	client := gocloak.NewClient(url)
	ctx := context.Background()

	var token *gocloak.JWT = nil
	var err error
	for i := 0; i < 5; i++ {
		log.Infof("Tring to connect to keycloak on URL %s for the %v. try", url, i)
		token, err = client.LoginAdmin(ctx, user, password, realm)
		if err != nil {
			log.Warning("Connection to keycloak Failed", err)
			if i < 4 {
				log.Info("Tring again in 10 seconds")
				time.Sleep(10 * time.Second)
			}
		}
	}
	if token == nil {
		log.Fatal("Could not connect to keycloak")
	}

	jwks, err := getJWKs(url, realm)
	if err != nil {
		panic(err)
	}
	keycloakConnection = KeycloakConnection{
		token:    token,
		client:   client,
		ctx:      ctx,
		realm:    realm,
		url:      url,
		user:     user,
		password: password,
		jwks:     jwks,
	}
}

func (k *KeycloakConnection) GetClient() (gocloak.GoCloak, error) {
	err := k.refreshTokenIfNeeded()
	if err != nil {
		return nil, errors.Wrap(err, "Failed Refreshing Token")
	}
	return k.client, nil
}

func (k KeycloakConnection) GetUsers() ([]*gocloak.User, error) {
	client, err := k.GetClient()
	if err != nil {
		return nil, err
	}
	users, err := client.GetUsers(k.ctx, k.token.AccessToken, k.realm, gocloak.GetUsersParams{})
	if err != nil {
		return nil, err
	}
	return users, err
}

func getJWKs(url, realm string) (*keyfunc.JWKs, error) {
	jwksURL := fmt.Sprintf("%s/auth/realms/%s/protocol/openid-connect/certs", url, realm)
	refreshInterval := time.Minute
	options := keyfunc.Options{
		RefreshInterval: &refreshInterval,
		RefreshErrorHandler: func(err error) {
			log.Printf("There was an error with the jwt.KeyFunc\nError: %s", err.Error())
		},
	}

	jwks, err := keyfunc.Get(jwksURL, options)
	if err != nil {
		log.Errorf("Failed to create JWKS from resource at the given URL.\nError: %s", err.Error())
		return nil, err
	}
	return jwks, nil
}
