package keycloak

import (
	"errors"

	"github.com/golang-jwt/jwt"
	log "github.com/sirupsen/logrus"
)

// Will error if the JWT is not valid
func (k KeycloakConnection) validateAndDecodeJWT(jwtB64 string) (*jwt.Token, error) {
	token, err := jwt.Parse(jwtB64, k.jwks.KeyFunc)
	if err != nil {
		log.Errorf("Failed to parse the JWT.\nError: %s", err.Error())
		return nil, err
	}
	if _, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return token, nil
	} else {
		log.Infoln("Received not valid token!")
		return nil, errors.New("Not Valid Token")
	}
}

func (k KeycloakConnection) IsValidJWT(jwtB64 string) (bool, error) {
	token, err := k.validateAndDecodeJWT(jwtB64)
	if err != nil {
		return false, err
	} else {
		return token.Valid, nil
	}
}

func (k KeycloakConnection) IsUserInJWTGroup(jwtB64 string, groupName string) (bool, error) {
	token, err := k.validateAndDecodeJWT(jwtB64)
	if err != nil {
		return false, err
	}
	claims := token.Claims.(jwt.MapClaims)

	if rec, ok := claims["groups"].([]interface{}); ok {
		for _, k := range rec {
			if k == groupName {
				return true, nil
			}
		}
	}
	return false, nil
}

func (k KeycloakConnection) ValidateAndGetUserId(jwtB64 string) (string, error) {
	v, err := k.validateAndDecodeJWT(jwtB64)
	if err != nil {
		return "", err
	}
	if sub, ok := v.Claims.(jwt.MapClaims)["sub"].(string); ok {
		return sub, nil
	}
	return "", errors.New("JWT is bad")
}

func (k KeycloakConnection) JwtHasVerifiedEmail(jwtB64 string) (bool, error) {
	v, err := k.validateAndDecodeJWT(jwtB64)
	if err != nil {
		return false, err
	}
	if verifiedEmail, ok := v.Claims.(jwt.MapClaims)["email_verified"].(bool); ok {
		return verifiedEmail, nil
	}
	return false, errors.New("JWT is bad")
}
