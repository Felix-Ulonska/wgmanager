package keycloak

import (
	"errors"

	"github.com/Nerzal/gocloak/v8"
)

const (
	ERR_USER_NOT_FOUND = "User not found"
)

// Returns *first* user with the mail
func (k *KeycloakConnection) GetUserByEmail(email string) (*gocloak.User, error) {
	client, err := k.GetClient()
	if err != nil {
		return nil, err
	}
	users, err := client.GetUsers(k.ctx, k.token.AccessToken, k.realm, gocloak.GetUsersParams{
		Email: &email,
	})
	if len(users) > 0 {
		return users[0], nil
	} else {
		return nil, errors.New(ERR_USER_NOT_FOUND)
	}
}

func (k *KeycloakConnection) GetUserById(id string) (*gocloak.User, error) {
	client, err := k.GetClient()
	if err != nil {
		return nil, err
	}
	return client.GetUserByID(k.ctx, k.token.AccessToken, k.realm, id)
}
