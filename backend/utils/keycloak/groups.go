package keycloak

import (
	"github.com/Nerzal/gocloak/v8"
	"github.com/pkg/errors"
)

func (k *KeycloakConnection) AvaibleGruops() ([]*gocloak.Group, error) {
	client, err := k.GetClient()
	if err != nil {
		return nil, err
	}
	return client.GetGroups(k.ctx, k.token.AccessToken, k.realm, gocloak.GetGroupsParams{})
}

func (k *KeycloakConnection) IsMemeberOfGroup(userId string, groupId string) (bool, error) {
	client, err := k.GetClient()
	if err != nil {
		return false, err
	}
	groups, err := client.GetUserGroups(k.ctx, k.token.AccessToken, k.realm, userId, gocloak.GetGroupsParams{})
	if err != nil {
		return false, err
	} else {
		for _, g := range groups {
			if groupId == *g.ID {
				return true, nil
			}
		}
	}
	return false, nil
}

func (k *KeycloakConnection) AddUserToGroup(userId string, groupId string) error {
	client, err := k.GetClient()
	if err != nil {
		return errors.Wrap(err, "Failed Adding User")
	}
	return client.AddUserToGroup(k.ctx, k.token.AccessToken, k.realm, userId, groupId)
}

func (k *KeycloakConnection) DeleteUserFromGroup(userId string, groupId string) error {
	client, err := k.GetClient()
	if err != nil {
		return err
	}
	return client.DeleteUserFromGroup(k.ctx, k.token.AccessToken, k.realm, userId, groupId)
}

func (k *KeycloakConnection) MembersOfGroup(groupId string) ([]*gocloak.User, error) {
	client, err := k.GetClient()
	if err != nil {
		return nil, err
	}
	return client.GetGroupMembers(k.ctx, k.token.AccessToken, k.realm, groupId, gocloak.GetGroupsParams{})
}

func (k *KeycloakConnection) GetGroups() ([]*gocloak.Group, error) {
	client, err := k.GetClient()
	if err != nil {
		return nil, err
	}
	return client.GetGroups(k.ctx, k.token.AccessToken, k.realm, gocloak.GetGroupsParams{})
}
