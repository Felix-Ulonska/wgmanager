package mailman3

import (
	"bytes"
	"encoding/json"
	"strconv"

	"github.com/pkg/errors"
)

type getListsResponseDto struct {
	Start     int              `json:"start"`
	TotalSize int              `json:"total_size"`
	Entries   []MailinglistDto `json:"entries"`
	HTTPEtag  string           `json:"http_etag"`
}

type addUserToMailinglistDto struct {
	ListID             string `json:"list_id"`
	Subscriber         string `json:"subscriber"`
	SendWelcomeMessage string `json:"send_welcome_message"`
	DeliveryMode       string `json:"delivery_mode,omitempty"`
	PreVerified        string `json:"pre_verified,omitempty"`
	PreConfirmed       string `json:"pre_confirmed,omitempty"`
	PreApproved        string `json:"pre_approved,omitempty"`
}

func (m *MailmanRestClient) GetAllLists() ([]MailinglistDto, error) {
	var listsDto getMailinglistResponseDto
	err := m.doJsonRequest("GET", LISTS_PATH, nil, &listsDto)
	if err != nil {
		return nil, errors.Wrap(err, "Failed GetAllLists")
	}
	return listsDto.Entries, nil
}

func (m *MailmanRestClient) AddUserToMailinglist(listId, subscriber string) error {
	dto := addUserToMailinglistDto{
		ListID:             listId,
		Subscriber:         subscriber,
		SendWelcomeMessage: "true",
		PreVerified:        "true",
		PreConfirmed:       "true",
		PreApproved:        "true",
	}
	jsonDto, err := json.Marshal(dto)
	if err != nil {
		return err
	}
	resp, err := m.doRequest("POST", MEMBERS_PATH, bytes.NewBuffer(jsonDto))
	defer resp.Body.Close()
	if resp.StatusCode != 201 {
		return errors.New("Adding user failed with status " + (strconv.Itoa(resp.StatusCode)))
	}
	return err
}

type getMailinglistResponseDto struct {
	Start     int              `json:"start"`
	TotalSize int              `json:"total_size"`
	Entries   []MailinglistDto `json:"entries"`
	HTTPEtag  string           `json:"http_etag"`
}
type MailinglistDto struct {
	Advertised   bool   `json:"advertised"`
	DisplayName  string `json:"display_name"`
	FqdnListname string `json:"fqdn_listname"`
	ListID       string `json:"list_id"`
	ListName     string `json:"list_name"`
	MailHost     string `json:"mail_host"`
	MemberCount  int    `json:"member_count"`
	Volume       int    `json:"volume"`
	Description  string `json:"description"`
	SelfLink     string `json:"self_link"`
	HTTPEtag     string `json:"http_etag"`
}

func (m *MailmanRestClient) getMaillistByEmail(email string) (MailinglistDto, error) {
	var getMailinglistResponse getMailinglistResponseDto
	err := m.doJsonRequest("GET", LISTS_PATH, nil, &getMailinglistResponse)
	if err != nil {
		return MailinglistDto{}, errors.Wrap(err, "Failed getting /lists")
	}
	for _, md := range getMailinglistResponse.Entries {
		if md.FqdnListname == email {
			return md, nil
		}
	}
	return MailinglistDto{}, errors.New("Not found")
}

type membersResponseDto struct {
	Start     int         `json:"start"`
	TotalSize int         `json:"total_size"`
	Entries   []MemberDto `json:"entries"`
	HTTPEtag  string      `json:"http_etag"`
}
type MemberDto struct {
	Address          string `json:"address"`
	DeliveryMode     string `json:"delivery_mode"`
	Email            string `json:"email"`
	ListID           string `json:"list_id"`
	SubscriptionMode string `json:"subscription_mode"`
	Role             string `json:"role"`
	User             string `json:"user"`
	ModerationAction string `json:"moderation_action,omitempty"`
	DisplayName      string `json:"display_name"`
	SelfLink         string `json:"self_link"`
	MemberID         string `json:"member_id"`
	HTTPEtag         string `json:"http_etag"`
}

func (m *MailmanRestClient) GetAllMembersOfMailinglist(listId string) ([]MemberDto, error) {
	var members membersResponseDto
	err := m.doJsonRequest("GET", MEMBERS_PATH, nil, &members)
	if err != nil {
		return nil, err
	}
	var membersInMl = make([]MemberDto, 0)
	for _, md := range members.Entries {
		if md.ListID == listId {
			membersInMl = append(membersInMl, md)
		}
	}
	return membersInMl, nil
}

type massUnsubscribeDto struct {
	Emails []string `json:"emails"`
}

func (m *MailmanRestClient) MassUnsubscribe(listId string, emails []string) error {
	dto := massUnsubscribeDto{Emails: emails}
	jsonDto, err := json.Marshal(dto)
	if err != nil {
		return err
	}
	_, err = m.doRequest("DELETE", API_BASE+"/lists/"+listId+"/roster/member", bytes.NewBuffer(jsonDto))
	return err
}

// Delete users with the selfLink!
func (m *MailmanRestClient) unsubscribe(selfLink string) error {
	_, err := m.doSelfLinkRequest("DELETE", selfLink, nil)
	return err
}

// This method will add subscriber if they are not subscribed and will remove all users which are not in the subscriber array
func (m *MailmanRestClient) BulkSetMembers(listId string, subscriber []string) error {
	members, err := m.GetAllMembersOfMailinglist(listId)
	if err != nil {
		return errors.Wrap(err, "Failed Getting AllMembers")
	}
	usersToDelete := make([]string, 0)
	for _, md := range members {
		exists := false
		for _, v := range subscriber {
			if md.Email == v {
				exists = true
			}
		}
		if !exists {
			usersToDelete = append(usersToDelete, md.SelfLink)
		}
	}

	for _, v := range usersToDelete {
		err := m.unsubscribe(v)
		if err != nil {
			return err
		}
	}

	if err != nil {
		return errors.Wrap(err, "massUnsubscribe failed in BulkSet")
	}

	usersToAdd := make([]string, 0)
	for _, v := range subscriber {
		inserted := false
		for _, md := range members {
			if md.Email == v {
				inserted = true
			}
		}
		if !inserted {
			usersToAdd = append(usersToAdd, v)
		}
	}

	for _, v := range usersToAdd {
		err := m.AddUserToMailinglist(listId, v)
		if err != nil {
			return errors.Wrap(err, "Failed adding User in BulkSet")
		}
	}

	return nil
}
