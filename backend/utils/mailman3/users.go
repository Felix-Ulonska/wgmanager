package mailman3

type usersResponseDto struct {
	Entries   []UserDto `json:"entries"`
	HTTPEtag  string    `json:"http_etag"`
	Start     int       `json:"start"`
	TotalSize int       `json:"total_size"`
}
type UserDto struct {
	CreatedOn     string `json:"created_on"`
	HTTPEtag      string `json:"http_etag"`
	IsServerOwner bool   `json:"is_server_owner"`
	Password      string `json:"password,omitempty"`
	SelfLink      string `json:"self_link"`
	UserID        string `json:"user_id"`
	DisplayName   string `json:"display_name,omitempty"`
}

func (m *MailmanRestClient) GetUsers() ([]UserDto, error) {
	var users *usersResponseDto
	err := m.doJsonRequest("GET", USER_PATH, nil, users)
	if err != nil {
		return nil, err
	}
	return users.Entries, nil
}
