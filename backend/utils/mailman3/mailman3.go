package mailman3

import (
	"encoding/base64"
	"encoding/json"
	"io"
	"net/http"
	netUrl "net/url"
	"strconv"
	"time"

	"github.com/pkg/errors"
)

type MailmanRestClient struct {
	host     string
	user     string
	password string
}

var (
	mailmanRestClient MailmanRestClient
	netClient         = &http.Client{Timeout: time.Second * 10}
)

const (
	API_BASE     = "/3.1"
	DOMAINS_PATH = API_BASE + "/domains"
	USER_PATH    = API_BASE + "/users"
	MEMBERS_PATH = API_BASE + "/members"
	LISTS_PATH   = API_BASE + "/lists"
)

func InitMailman(host, username, password string) {
	mailmanRestClient = MailmanRestClient{
		host:     host,
		user:     username,
		password: password,
	}
}

func GetMailmanRestClient() *MailmanRestClient {
	return &mailmanRestClient
}

// Thanks https://stackoverflow.com/questions/16673766/basic-http-auth-in-go
func basicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

func (m *MailmanRestClient) doRequest(method, url string, body io.Reader) (*http.Response, error) {
	return m.doRequestWithFullUrl(method, m.host+url, body)
}

func (m *MailmanRestClient) doSelfLinkRequest(method, url string, body io.Reader) (*http.Response, error) {
	u, err := netUrl.Parse(url)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to parse self link")
	}
	return m.doRequest(method, u.Path, body)
}

func (m *MailmanRestClient) doRequestWithFullUrl(method, url string, body io.Reader) (*http.Response, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", "Basic "+basicAuth(m.user, m.password))
	if body != nil {
		req.Header.Add("Content-Type", "application/json")
	}
	return netClient.Do(req)
}

// Request with JSON as Response
func (m *MailmanRestClient) doJsonRequest(method, url string, body io.Reader, v interface{}) error {
	resp, err := m.doRequest(method, url, body)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	retBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	if err := json.Unmarshal(retBody, &v); err != nil {
		return err
	}
	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		return errors.New("Unexpected Statuscode " + strconv.Itoa(resp.StatusCode))
	}
	return nil
}
