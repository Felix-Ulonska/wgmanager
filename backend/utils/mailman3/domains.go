package mailman3

import (
	"strings"
)

type domainsDtoType struct {
	Start     int      `json:"start"`
	TotalSize int      `json:"total_size"`
	Entries   []Domain `json:"entries"`
	HTTPEtag  string   `json:"http_etag"`
}

type Domain struct {
	AliasDomain string `json:"alias_domain"`
	Description string `json:"description"`
	MailHost    string `json:"mail_host"`
	SelfLink    string `json:"self_link"`
	HTTPEtag    string `json:"http_etag"`
}

func (m *MailmanRestClient) Domains() ([]Domain, error) {
	var domainsDto domainsDtoType
	err := m.doJsonRequest("GET", DOMAINS_PATH, strings.NewReader(""), &domainsDto)
	if err != nil {
		return nil, err
	}
	return domainsDto.Entries, nil
}
