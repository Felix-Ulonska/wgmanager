package mailman3

import (
	"time"

	"github.com/Nerzal/gocloak/v8"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"

	configeditor "gitlab.com/Felix-Ulonska/wgmanager/utils/config_editor"
	"gitlab.com/Felix-Ulonska/wgmanager/utils/keycloak"
)

func StartBackgroundSync(refreshInterval time.Duration) {
	go syncLoop(refreshInterval)
}

func syncLoop(refreshInterval time.Duration) {
	startTime := time.Now()
	for {
		for time.Now().UnixNano()-startTime.UnixNano() < refreshInterval.Nanoseconds() {
			time.Sleep(time.Second * 1)
		}
		startTime = time.Now()
		syncFailed := GetMailmanRestClient().sync()
		if syncFailed != nil {
			log.Warn("Sync failed!", syncFailed.Error())
		}
		checkForMissingGroupsError := checkForMissingGroups()
		if checkForMissingGroupsError != nil {
			log.Warn("Failed check for valid data set", checkForMissingGroupsError)
		}
	}
}

type GroupWithMailinglist interface {
	MailinglistMembers() ([]*gocloak.User, error)
	Listaddress() string
	IsValid() bool
}

func checkForMissingGroups() error {
	groups, err := keycloak.GetKeycloak().GetGroups()
	if err != nil {
		errors.Wrap(err, "Failed checkGroupValid")
	}
	configeditor.GetConfig().RefreshValidStatusMailinglists(groups)
	configeditor.GetConfig().RefreshValidStatusWorkgroups(groups)
	return nil
}

func (m *MailmanRestClient) sync() error {
	workgroups := configeditor.GetConfig().Workgroups()
	mailinglists := configeditor.GetConfig().Mailinglists()
	thingsWithMembers := make([]GroupWithMailinglist, 0)
	for _, w := range workgroups {
		thingsWithMembers = append(thingsWithMembers, w)
	}
	for _, m := range mailinglists {
		thingsWithMembers = append(thingsWithMembers, m)
	}
	validGroupWithMailinglist := make([]GroupWithMailinglist, 0)

	for _, gwm := range thingsWithMembers {
		if gwm.IsValid() {
			validGroupWithMailinglist = append(validGroupWithMailinglist, gwm)
		}
	}

	for _, m2 := range validGroupWithMailinglist {
		subscriber := make([]string, 0)
		users, err := m2.MailinglistMembers()
		if err != nil {
			return errors.Wrap(err, "Getting Members failed")
		}
		for _, u := range users {
			subscriber = append(subscriber, *u.Email)
		}
		if m2.Listaddress() != "" {
			list, err := m.getMaillistByEmail(m2.Listaddress())
			if err != nil {
				return errors.Wrap(err, "Getting Mailinglist by ID failed")
			}
			err = m.BulkSetMembers(list.ListID, subscriber)
			if err != nil {
				return errors.Wrap(err, "BulkSet failed")
			}
		}
	}

	return nil
}
