package mail

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"html/template"
	"strings"

	log "github.com/sirupsen/logrus"

	"github.com/emersion/go-sasl"
	"github.com/emersion/go-smtp"
	"gitlab.com/Felix-Ulonska/wgmanager/utils/keycloak"
)

type SmtpConnection struct {
	senderMail string
	user       string
	password   string
	smtpHost   string
}

var (
	smtpConnection       SmtpConnection
	mailtemplateLocation string
)

func GetSmtpConnection() *SmtpConnection {
	return &smtpConnection
}

func InitMail(user, password, smtpHost, senderMail, pMailtemplateLocation string) {
	smtpConnection = SmtpConnection{
		senderMail: senderMail,
		user:       user,
		password:   password,
		smtpHost:   smtpHost,
	}
	mailtemplateLocation = pMailtemplateLocation
	// Connect to the server, authenticate, set the sender and recipient,
	// and send the email all in one step.
}

func (smtpConnection *SmtpConnection) SendMail(recipient, body string) error {
	auth := sasl.NewPlainClient("", smtpConnection.user, smtpConnection.password)
	to := []string{recipient}
	msgStr := fmt.Sprintf("From: %v\r\n"+
		"To: %v\r\n"+
		"Content-Type: text/html; charset=\"utf-8\"\r\n"+
		"Content-transfer-encoding: base64\r\n"+
		"%v\r\n", smtpConnection.senderMail, recipient, body)
	msg := strings.NewReader(msgStr)
	err := smtp.SendMail(smtpConnection.smtpHost, auth, smtpConnection.senderMail, to, msg)
	if err != nil {
		log.Warn(err)
	}
	return nil
}

func (smtpConnection *SmtpConnection) SendMailTemplate(userId, templatePath string, data templateBase) error {
	log.Debugf("Sending Mail with template %v to user %v", templatePath, userId)
	data.addReceipantInfo(userId)
	user, err := keycloak.GetKeycloak().GetUserById(userId)
	if err != nil {
		return err
	}
	t, err := template.ParseFiles(
		mailtemplateLocation+baseTemplate,
		mailtemplateLocation+greetingTemplate,
		mailtemplateLocation+footerTemplate,
		mailtemplateLocation+templatePath,
	)
	if err != nil {
		return err
	}
	var tpl bytes.Buffer
	err = t.Execute(&tpl, data)
	if err != nil {
		return err
	}
	return GetSmtpConnection().SendMail(*user.Email, fixEmailEncoding(tpl.String()))
}

// SMTP only support line length to 1000 chars. Therefore the email body is converted to base64. The first line is the subject which needs to be ignored
func fixEmailEncoding(email string) string {
	firstLinbreak := strings.Index(email, "\n")
	subject := email[:firstLinbreak]
	body := email[firstLinbreak:]
	return subject + "\r\n" + base64.StdEncoding.EncodeToString([]byte(body))
}

func (smtpConnection *SmtpConnection) getEmailOfUser(userId string) (string, error) {
	user, err := keycloak.GetKeycloak().GetUserById(userId)
	if err != nil {
		return "", err
	}
	return *user.Email, nil
}
