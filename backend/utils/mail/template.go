package mail

import (
	"github.com/Nerzal/gocloak/v8"
	"gitlab.com/Felix-Ulonska/wgmanager/utils/keycloak"
)

const (
	greetingTemplate              = "/greetings.html"
	baseTemplate                  = "/base.html"
	footerTemplate                = "/footer.html"
	speakerNewMemberInGroup       = "/speaker/newMember.html"
	speakerMemberRemovedFromGroup = "/speaker/removedMember.html"
	userAddedToGroup              = "/user/addedToGroup.html"
	userRemovedFromGroupTemplate  = "/user/removedFromGroup.html"
)

type GreetingsTemplate struct {
	RecipientFirstname string
	RecipientLastname  string
}

type templateBase interface {
	addReceipantInfo(string) error
}

type FooterTemplate struct{}

type BaseTemplate struct {
	GreetingsTemplate
	FooterTemplate
}

type AddGroupNotificationSpeakerTemplate struct {
	BaseTemplate
	NewUserFirstName string
	NewUserLastName  string
	WgName           string
}

type RemoevedUserFromGroupNotificationSpeakerTemplate struct {
	BaseTemplate
	RemovedUserFirstName string
	RemovedUserLastName  string
	WgName               string
}

func (b *BaseTemplate) addReceipantInfo(userId string) error {
	user, err := keycloak.GetKeycloak().GetUserById(userId)
	if err != nil {
		return err
	}
	b.RecipientFirstname = *user.FirstName
	b.RecipientLastname = *user.LastName
	return nil
}

/**
-------------------
+     SPEAKER    ++
-------------------
*/

func (smtpConnection *SmtpConnection) SendSpeakerNewMemberNotification(newUserId string, wgName string, speaker []*gocloak.User) error {
	newUser, err := keycloak.GetKeycloak().GetUserById(newUserId)
	if err != nil {
		return err
	}
	for _, u := range speaker {
		smtpConnection.SendMailTemplate(*u.ID, speakerNewMemberInGroup, &AddGroupNotificationSpeakerTemplate{
			NewUserFirstName: *newUser.FirstName,
			NewUserLastName:  *newUser.LastName,
			WgName:           wgName,
		})
	}

	return err
}

func (smtpConnection *SmtpConnection) SendSpeakerMemberRemovedNotification(removedUserId string, wgName string, speaker []*gocloak.User) error {
	newUser, err := keycloak.GetKeycloak().GetUserById(removedUserId)
	if err != nil {
		return err
	}
	for _, u := range speaker {
		smtpConnection.SendMailTemplate(*u.ID, speakerMemberRemovedFromGroup, &RemoevedUserFromGroupNotificationSpeakerTemplate{
			RemovedUserFirstName: *newUser.FirstName,
			RemovedUserLastName:  *newUser.LastName,
			WgName:               wgName,
		})
	}

	return err
}

/**
-------------------
+     USER       ++
-------------------
*/
type addedToGroupTemplate struct {
	BaseTemplate
	WgName string
}

func (smtpConnection *SmtpConnection) SendUserAddedToGroupMail(userId string, wgName string) error {
	return smtpConnection.SendMailTemplate(userId, userAddedToGroup, &addedToGroupTemplate{WgName: wgName})
}

type removedFromGroupTemplate struct {
	BaseTemplate
	WgName string
}

func (smtpConnection *SmtpConnection) SendUserRemovedFromGroupMail(userId string, wgName string) error {
	return smtpConnection.SendMailTemplate(userId, userRemovedFromGroupTemplate, &removedFromGroupTemplate{WgName: wgName})
}
