package configeditor

import (
	"github.com/Nerzal/gocloak/v8"
	"github.com/barkimedes/go-deepcopy"
	"github.com/sirupsen/logrus"
	"gitlab.com/Felix-Ulonska/wgmanager/models"
)

type WorkgroupNotFoundError struct{}

func (m *WorkgroupNotFoundError) Error() string {
	return "The Workgroup is not found"
}

func (c *ConfigEditor) EditWorkgroup(editedWorkgroup models.Workgroup) error {
	return c.runConfigOp(func(config models.Config) (models.Config, error) {
		for i, w := range config.Workgroups {
			if w.UUID == editedWorkgroup.UUID {
				config.Workgroups[i] = editedWorkgroup
				return config, nil
			}
		}
		return models.Config{}, &WorkgroupNotFoundError{}
	})
}

/// Returns deepcopied copy of Mailinglists.
func (c *ConfigEditor) Workgroups() []models.Workgroup {
	copied := deepcopy.MustAnything(c.config.Workgroups)
	return copied.([]models.Workgroup)
}

// Returns Mailinglist. If it does not exist, returns MailinglistNotFoundError
func (c *ConfigEditor) Workgroup(uuid string) (models.Workgroup, error) {
	for _, w := range c.config.Workgroups {
		if w.UUID == uuid {
			return w, nil
		}
	}
	return models.Workgroup{}, &WorkgroupNotFoundError{}
}

// Will add new workgroup to the config. Will set UUID field!
func (c *ConfigEditor) AddWorkgoup(newWorkgroup models.Workgroup) (models.Workgroup, error) {
	err := c.runConfigOp(func(config models.Config) (models.Config, error) {
		config.Workgroups = append(config.Workgroups, newWorkgroup)
		return config, nil
	})
	if err != nil {
		return models.Workgroup{}, err
	} else {
		return newWorkgroup, nil
	}
}

func (c *ConfigEditor) DeleteWorkgroup(uuid string) error {
	return c.runConfigOp(func(config models.Config) (models.Config, error) {
		for i, w := range config.Workgroups {
			if w.UUID == uuid {
				logrus.Warn("Delete " + uuid)
				// Swap with last element and return without last element
				config.Workgroups[i] = config.Workgroups[len(config.Workgroups)-1]
				config.Workgroups = config.Workgroups[:len(config.Workgroups)-1]
				return config, nil
			}
		}
		return models.Config{}, &WorkgroupNotFoundError{}
	})
}

func (c *ConfigEditor) getAllUsedMailsByWorkgroups() []string {
	usedMails := make([]string, 0)
	for _, w := range c.config.Workgroups {
		usedMails = append(usedMails, w.Mail)
	}
	return usedMails
}

func (c *ConfigEditor) RefreshValidStatusWorkgroups(groups []*gocloak.Group) {
	c.runConfigOp(func(config models.Config) (models.Config, error) {
		for i, w := range config.Workgroups {
			w.RefreshValid(groups)
			config.Workgroups[i] = w
		}
		return config, nil
	})
}
