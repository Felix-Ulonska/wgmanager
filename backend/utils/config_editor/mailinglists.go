package configeditor

import (
	"github.com/Nerzal/gocloak/v8"
	"github.com/barkimedes/go-deepcopy"
	log "github.com/sirupsen/logrus"
	"gitlab.com/Felix-Ulonska/wgmanager/models"
)

/// Returns deepcopied copy of Mailinglists.
func (c *ConfigEditor) Mailinglists() []models.Mailinglist {
	copied := deepcopy.MustAnything(c.config.Mailinglists)
	return copied.([]models.Mailinglist)
}

// Will add new mailinglist to the config. Will set UUID field!
func (c *ConfigEditor) AddMailinglist(newMailinglist models.Mailinglist) models.Mailinglist {
	c.runConfigOp(func(config models.Config) (models.Config, error) {
		config.Mailinglists = append(config.Mailinglists, newMailinglist)
		return config, nil
	})
	return deepcopy.MustAnything(newMailinglist).(models.Mailinglist)
}

type MailinglistNotFoundError struct{}

func (m *MailinglistNotFoundError) Error() string {
	return "The Mailinglist is not found"
}

// Returns Mailinglist. If it does not exist, returns MailinglistNotFoundError
func (c *ConfigEditor) Mailinglist(uuid string) (models.Mailinglist, error) {
	for _, m := range c.config.Mailinglists {
		if m.UUID == uuid {
			return m, nil
		}
	}
	return models.Mailinglist{}, &MailinglistNotFoundError{}
}

func (c *ConfigEditor) EditMailinglist(editedMailinglist models.Mailinglist) error {
	return c.runConfigOp(func(config models.Config) (models.Config, error) {
		for i, m := range config.Mailinglists {
			if m.UUID == editedMailinglist.UUID {
				config.Mailinglists[i] = editedMailinglist
				log.Info("Seting config")
				return config, nil
			}
		}
		return models.Config{}, &MailinglistNotFoundError{}
	})
}

func (c *ConfigEditor) DeleteMailinglist(uuid string) error {
	return c.runConfigOp(func(config models.Config) (models.Config, error) {
		for i, m := range config.Mailinglists {
			if m.UUID == uuid {
				// Swap with last element and return without last element
				config.Mailinglists[i] = config.Mailinglists[len(config.Mailinglists)-1]
				config.Mailinglists = config.Mailinglists[:len(config.Mailinglists)-1]
				return config, nil
			}
		}
		return models.Config{}, &MailinglistNotFoundError{}
	})
}

func (c *ConfigEditor) getAllUsedMailsForMaillists() []string {
	usedMails := make([]string, 0)
	for _, m := range c.config.Mailinglists {
		usedMails = append(usedMails, m.Email)
	}
	return usedMails
}

func (c *ConfigEditor) RefreshValidStatusMailinglists(groups []*gocloak.Group) {
	c.runConfigOp(func(config models.Config) (models.Config, error) {
		for i, m := range config.Mailinglists {
			m.RefreshValid(groups)
			config.Mailinglists[i] = m
		}
		return config, nil
	})
}
