package configeditor

import (
	"encoding/json"
	"io/ioutil"
	"sync"

	log "github.com/sirupsen/logrus"

	"github.com/barkimedes/go-deepcopy"
	"gitlab.com/Felix-Ulonska/wgmanager/models"
)

var (
	configEditor ConfigEditor
)

func InitConfig(path string) {
	configEditor = loadConfig(path)
}

func GetConfig() *ConfigEditor {
	return &configEditor
}

type ConfigEditor struct {
	config *models.Config
	path   string
	mu     sync.Mutex
}

// Decorator which handels config Operations. Should be used to prevent race conditions and to save the Config
// When configOp returns a non-null error the new config is discarded
func (c *ConfigEditor) runConfigOp(configOp func(models.Config) (models.Config, error)) error {
	c.mu.Lock()
	defer c.mu.Unlock()
	newConfig, err := configOp(*c.config)
	if err != nil {
		return err
	}
	c.config = &newConfig
	return c.saveConfig()
}

func loadConfig(configPath string) ConfigEditor {
	log.Info("Loading config file")
	dat, err := ioutil.ReadFile(configPath)

	var config models.Config

	if err != nil {
		// TODO Make sure this does not overwrite old config
		log.Warning("Config cannot be opened; creating NEW config", err)
		config = models.Config{Mailinglists: make([]models.Mailinglist, 0), Workgroups: make([]models.Workgroup, 0)}
	} else {
		log.Infof("Loaded Config from %v", configPath)
		json.Unmarshal(dat, &config)
	}

	return ConfigEditor{&config, configPath, sync.Mutex{}}
}

/// Returns deepcopied copy of Mailinglists.
func (c *ConfigEditor) Groups() ([]models.LdapGroup, error) {
	copied, err := deepcopy.Anything(c.config.Workgroups)
	if err != nil {
		return nil, err
	}
	return copied.([]models.LdapGroup), nil
}

func (c *ConfigEditor) saveConfig() error {
	file, err := json.MarshalIndent(c.config, "", " ")
	if err != nil {
		log.Error("Unable to marshel json in SaveConfig", err)
		return err
	}
	err = ioutil.WriteFile(c.path, file, 0644)
	if err != nil {
		log.Error("Cannot write config", err)
		return err
	}
	return nil
}

// will delete everything in the config. Should only use for testing.
func (c *ConfigEditor) Reset() error {
	log.Info("Reseting Config!")
	return c.runConfigOp(func(_ models.Config) (models.Config, error) {
		mailinglists := make([]models.Mailinglist, 1)
		mailinglists[0] = models.Mailinglist{
			UUID:                   "uuid",
			Email:                  "test@example.org",
			Name:                   "testForEdit",
			ModifierGroup:          "9b7b014a-0567-41cc-b4cf-b0d1e1e1304e",
			BaseGroup:              "0d307712-5d2f-4da6-b5ea-751309272324",
			ModifierGroupIsInclude: false,
		}
		workgroups := make([]models.Workgroup, 1)
		workgroups[0] = models.Workgroup{
			UUID:                        "b8f15926-d3a3-426d-a340-6d417b2d9ffb",
			Name:                        "testGroup",
			AllowJoinLdapGroup:          "0d307712-5d2f-4da6-b5ea-751309272324",
			MemberLdapGroup:             "3e426d60-4dfa-467e-a285-889ec9e015db",
			ExcludeMailinglistLdapGroup: "9b7b014a-0567-41cc-b4cf-b0d1e1e1304e",
			Mail:                        "hallo@web.de",
			MatterMostLink:              "ein.link",
		}
		return models.Config{
			Mailinglists: mailinglists,
			Workgroups:   workgroups,
		}, nil
	})
}

func (c *ConfigEditor) GetAllUsedMails() []string {
	return append(c.getAllUsedMailsForMaillists(), c.getAllUsedMailsByWorkgroups()...)
}
