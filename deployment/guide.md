# Deployment Guide

## Variables

- $HOSTING_LOCATION: place from where the application is served
env-file:
```
ADMIN_GROUP_PATH="/wg_admin"
KEYCLOAK_USER="wgbackend"
KEYCLOAK_PASS="pass123"
KEYCLOAK_REALM="WgManager"
KEYCLOAK_PATH="http://localhost:8080"
SMTP_HOST="localhost:1025"
SMTP_PASS="pass"
SMTP_USER="user"
SMTP_SENDER_MAIL="test@example.org"
MAILMAN_REST_USER="restadmin"
MAILMAN_REST_PASSWORD="restpass"
MAILMAN_REST_HOST="http://localhost:8001"
```

## Prerequisites

### Keycloak

1. Set the variable `KEYCLOAK_PATH` with the location of the keycloak
2. Create or use an existing realm and set the variable `KEYCLOAK_REALM`
3. Create a backend User to manage the realm with the following client roles and set the variables `KEYCLOAK_USER` and `KEYCLOAK_PASS`:
	- manage-users
	- query-groups
	- query-realms
	- query-users
	- view-users
4. Create a Admin Group and set `ADMIN_GROUP_PATH` with the **path** of the group
5. Create a Client with, default settings if not otherwise mentioned:
	- Choose a good name. This is not relevant
	- Client Protocol: openid-connect
	- Root url: $HOSTING_LOCATION
	- create a group mapper:
		- **set token clain name to `groups`**
		- set all toggles to true
		![Screenshot of Group Creation in keycloak, relevant information is also present in markdown](./tutorial_images/keycloak_mapper.png "Keycloak Group Creation")
	- switch to tab `Installation` and select format `Keycloak OIDC JSON`, download the json

### SMTP

- Supply an SMTP Mailserver and set:
	- `SMTP_HOST`
	- `SMTP_USER`
	- `SMTP_PASS`
	- `SMTP_SENDER_MAIL`

### Mailman3

- Supply login data for the API and set:
	- `MAILMAN_REST_HOST`
	- `MAILMAN_REST_PASSWORD`
	- `MAILMAN_REST_USER`

## Deployment

- Run 
```bash
curl https://gitlab.com/Felix-Ulonska/wgmanager/-/raw/main/deployment/docker-compose.yml > docker-compose.yml
curl https://gitlab.com/Felix-Ulonska/wgmanager/-/raw/main/deployment/.env > .env
touch config.json
```
- Fill the .env file with the information from the Prerequisites
- Start the stack `docker-compose up -d` -d


