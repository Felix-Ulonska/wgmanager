# wgManager

## LICENSE

The source code is licensed under GPLv3.

## Installation

Run `make install`

### Start

Run `make run_frontend` to start the frontend
Run `make run_backend` to start the backend

### API

Run `make gen_api` to generate the api.spec from the backend and to build the frontend files.

## Backend

The Backend is written in go. The api is documentated with `swag`. To generate the `api.yaml` run `swag init` within the backend folder.

## Frontend

The Frontend is written in Angular and Angular Material.
