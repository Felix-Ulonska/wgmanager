install:
	go get -u github.com/swaggo/swag/cmd/swag
	cd backend; swag init; go get
	cd frontend; npm i

gen_api:
	cd backend; swag init; cd ..
	cd frontend; npm run gen:api; cd ..

gen_api_bedrock: 
	cd backend; strat arch swag init; cd ..
	cd frontend; npm run gen:api; cd ..


run_keycloak: reset_keycloak
	cd testdata; docker-compose up -d keycloak 

stop_keycloak:
	cd testdata; docker-compose down

reset_keycloak: stop_keycloak
	cd testdata; docker-compose rm  -f -s -v || true

run_mail:
	cd testdata; docker-compose up -d mail

run_frontend:
	cd frontend; npx ng serve --proxy-config proxy-config.json --port 8000

run_backend:
	cd backend; go run main.go

run_whole_stack: run_keycloak
	cd backend; go run main.go &
	cd frontend; npx ng serve --proxy-config proxy-config.json --port 8000

run_cypress: 
	cd frontend; \
	npx wait-on http://localhost:8080 -t 120000; \
	npx wait-on http://localhost:8000 -t 120000; \
	npm ci; \
	npx cypress run --reporter junit --reporter-options "mochaFile=results/my-test-output-[hash].xml"

clean:
	rm -rf frontend/node_modules
	rm -rf backend/docs

all: install getn_api run_keycloak run_whole_stack
